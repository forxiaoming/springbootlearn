# Springboot 权限整合学习

## Spring Security

* 是一个功能强大且高度可定制的身份验证和访问控制框架。
* 它是用于保护基于Spring的应用程序的实际标准。
* Spring Security是一个框架，致力于为Java应用程序提供身份验证和授权。
* 与所有Spring项目一样，Spring Security的真正强大之处在于可以轻松扩展以满足自定义要求

### Spring Security Guides

* [./gs-securing-web](./gs-securing-web)

### Spring Boot and OAuth2

* https://spring.io/guides/tutorials/spring-boot-oauth2/

## 参考

* [理解OAuth 2.0 - 阮一峰](https://www.ruanyifeng.com/blog/2014/05/oauth_2_0.html)
* [JSON Web Token 入门教程 - 阮一峰](http://www.ruanyifeng.com/blog/2018/07/json_web_token-tutorial.html)