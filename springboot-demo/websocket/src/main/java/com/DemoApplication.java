package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        // 来源参考 https://blog.cnbuilder.cn/archives/SpringBoot_websocket
        SpringApplication.run(DemoApplication.class, args);
    }

}
