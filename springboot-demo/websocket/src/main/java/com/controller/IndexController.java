package com.controller;

import com.ProductWebSocket;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 发送服务器
 **/
@Controller
public class IndexController {

    @GetMapping(value = "/")
    @ResponseBody
    public Object index(){

        return "Hello, This is WebSocket Demo!";
    }

    @GetMapping(value = "test")
    @ResponseBody
    public String test(@RequestParam String userId,
                       @RequestParam String message) {
        if ( userId == null || "".equals(userId) ) {
            return "发送userId不能为空";
        }

        if ( message == null || "".equals(message) ) {
            return "发送message不能为空";
        }
        "".replace("s","d");
        new ProductWebSocket().systemSendToUser(userId, message);

        return "发送成功!";
    }

    @RequestMapping(value = "/ws")
    public String ws(){
        return "ws";
    }

    @RequestMapping(value = "/ws1")
    public String ws1() {
        return "ws1";
    }


}