import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author xiaoming
 * @version 1.0
 * @date 2019-08-11 12:48
 **/
public class Test {

    public static void main(String[] args) {
        AtomicInteger a = new AtomicInteger(1000);
        System.out.println(a.getAndIncrement());
        System.out.println(a.getAndIncrement());

    }
}
