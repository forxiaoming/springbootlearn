package demo.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author xiaoming
 * @version 1.0
 * @date 2019-04-20 17:49
 **/
public class ErrorInfo<T> {
    public static final Integer OK = 0;
    public static final Integer ERROR = 1;

    @Getter @Setter private Integer code;
    @Getter @Setter private String message;
    @Getter @Setter private String url;
    @Getter @Setter private T data;


}
