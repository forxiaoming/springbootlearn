package demo;

import demo.exception.MyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xiaoming
 * @version 1.0
 * @date 2019-04-20 18:02
 **/
@Controller
@RequestMapping("/greeting")
public class GreetingController {
    @GetMapping(value = "/")
    public String geeting(@RequestParam(value = "name",required = false,defaultValue = "world") String name,
                          Model model) {
        System.out.println("name: " + name);
        model.addAttribute("name", name);
        return "greeting";
    }

    @GetMapping(value = "/error")
    public String error() throws Exception {
        System.out.println("error");
        throw new Exception("发生了错误异常");
    }

    @GetMapping(value = "/jsonError")
    public String errorJSON() throws MyException {
        throw new MyException("JSON格式错误");
    }
}
