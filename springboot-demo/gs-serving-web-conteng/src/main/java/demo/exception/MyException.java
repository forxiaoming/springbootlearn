package demo.exception;

/**
 * @author xiaoming
 * @version 1.0
 * @date 2019-04-20 17:47
 **/

public class MyException extends Exception {
    public MyException(String message) {
        super(message);
    }
}
