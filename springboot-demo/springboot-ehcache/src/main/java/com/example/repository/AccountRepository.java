package com.example.repository;

import com.example.entity.Account;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author xiaoming
 * @version 1.0
 * @date 2019-04-28 22:56
 **/
@CacheConfig(cacheNames = "account")
public interface AccountRepository extends JpaRepository<Account, Integer> {
    @Cacheable
    Account findAccountById(int id);

}
