package com.example.controller;

import com.example.entity.Account;
import com.example.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author xiaoming
 * @version 1.0
 * @date 2019-04-28 22:56
 **/
@RestController
@RequestMapping(value = "/account")
public class AccountController {

    @Autowired
    private AccountRepository accountRepository;

    // 正常查询
    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<Account> getAll() {
        return accountRepository.findAll();
    }

    // 开启缓存的查询
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Account getByIdOrCache(@PathVariable(value = "id") int id) {

        return accountRepository.findAccountById(id);
    }


}
