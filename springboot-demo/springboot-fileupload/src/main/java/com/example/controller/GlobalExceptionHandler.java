package com.example.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * 异常处理
 * @author xiaoming
 * @version 1.0
 * @date 2019-06-02 8:47
 **/
@ControllerAdvice
public class GlobalExceptionHandler {


    @ExceptionHandler(MultipartException.class)
    public String handlerMultipartError(MultipartException m, RedirectAttributes attributes) {
        // 处理如文件大小超出异常,FileSizeLimitExceededException

        attributes.addFlashAttribute("message", m.getCause().getMessage());

        return "redirect:/uploadStatus";
    }
}
