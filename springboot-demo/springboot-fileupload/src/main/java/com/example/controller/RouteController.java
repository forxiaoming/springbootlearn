package com.example.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author xiaoming
 * @version 1.0
 * @date 2019-05-01 20:44
 **/
@Controller
public class RouteController {
    private Log log = LogFactory.getLog(RouteController.class);
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String toIndex() {
        log.info("to upload");
        return "upload";
    }

    @GetMapping(value = "/uploadStatus")
    public String toUploadStatus() {
        return "uploadStatus";
    }

}
