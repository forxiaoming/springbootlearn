package com.example.controller.qiniu.oos.util;

import com.qiniu.util.Auth;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author xiaoming
 * @version 1.0
 * @date 2019-06-02 10:02
 **/
public class QiNiuyUtil {
    private static Log log = LogFactory.getLog(QiNiuyUtil.class);


    public static String  getUploadToken() {

        String accessKey = "access key";
        String secretKey = "secret key";
        String bucket = "bucket name";

        Auth auth = Auth.create(accessKey, secretKey);
        String upToken = auth.uploadToken(bucket);


        log.info("获取凭证: " + upToken);

        return upToken;
    }
}
