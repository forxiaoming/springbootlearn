package com.example.controller.qiniu.oos.server.upload;

import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.common.Region;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * 文件上传
 * @author xiaoming
 * @version 1.0
 * @date 2019-06-02 10:21
 **/
public class UploadLocalFile {
    private static Log log = LogFactory.getLog(UploadLocalFile.class);

    public static void main(String[] args) {
        //构造一个带指定Zone对象的配置类

        // 华南区 Region.region1()
        Configuration cfg = new Configuration(Region.region2());
        //...其他参数参考类注释

        UploadManager uploadManager = new UploadManager(cfg);
        //...生成上传凭证，然后准备上传

        String accessKey = "xxdTwX3v2X1c8ml-xwFD7saUIW3fWALhoQuByre-";
        String secretKey = "-LoCyCeUJOOUIuAqm4ncbMn-5onjgBCyzCoTVpbm";
        String bucket = "filestoragetest";

        //如果是Windows情况下，格式是 D:\\qiniu\\test.png
        //String localFilePath = "/home/qiniu/test.png";
        String localFilePath = "D:\\DFS\\tes2_1M.gif";

        //默认不指定key的情况下，以文件内容的hash值作为文件名
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String key = "timpImg/" + df.format(new Date()) + "-" + UUID.randomUUID().toString();

        log.info("key: " + key);

        Auth auth = Auth.create(accessKey, secretKey);
        String upToken = auth.uploadToken(bucket);

        log.info("Token: " + upToken);

        System.out.println("Token: " + upToken);



        try {
            Response response = uploadManager.put(localFilePath, key, upToken);
            //解析上传成功的结果
            DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);

            System.out.println(putRet.key);

            System.out.println(putRet.hash);

        } catch (QiniuException ex) {
            Response r = ex.response;
            System.err.println(r.toString());
            try {
                System.err.println(r.bodyString());
            } catch (QiniuException ex2) {
                //ignore
            }
        }

    }
}
