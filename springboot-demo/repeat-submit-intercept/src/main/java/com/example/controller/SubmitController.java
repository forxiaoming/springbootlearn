package com.example.controller;

import com.example.aop.NoRepeatSubmit;
import com.example.utils.ApiResult;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SubmitController {

    @PostMapping(value = "submit")
    @NoRepeatSubmit(lockTime = 30)
    public Object submit(@RequestBody UserBean userBean) {
        try {
            // 模拟业务场景
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return new ApiResult(200, "提交成功", userBean.getUserId());

    }


    public static class UserBean {
        @Getter
        @Setter
        private String userId;

    }
}
