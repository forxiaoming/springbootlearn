package com.example.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class RunTest implements ApplicationRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(RunTest.class);

    @Autowired
    private RestTemplate restTemplate ;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        LOGGER.info("多线程测试");
        String url = "http://localhost/submit";
        // 允许一个或多个线程等待直到在其他线程中执行的一组操作完成的同步辅助。
        // 实现类似计数器的功能
        CountDownLatch countDownLatch = new CountDownLatch(1);
        System.out.println("latch: " + countDownLatch.getCount());
        ExecutorService executorService = Executors.newFixedThreadPool(10);

        for (int i = 0; i < 10; i++) {
            String userId = "userId" + i;
            HttpEntity<String> request = buildRequest(userId);
            executorService.submit(() -> {

                try {
                    // 挂起线程, 直到 count为0
                    countDownLatch.await();
                    System.out.println("Thread: " + Thread.currentThread().getName() + ", time: " + System.currentTimeMillis());
                    ResponseEntity<String> response = restTemplate.postForEntity(url, request, String.class);
                    System.out.println("Thread: " + Thread.currentThread().getName() + ", body: " + response.getBody());

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


            });



        }

        countDownLatch.countDown();

    }

    private HttpEntity buildRequest(String userId) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "yourToken");

        Map<String, Object> body = new HashMap<>();
        body.put("userId", userId);

        return new HttpEntity(body, headers);
    }
}
