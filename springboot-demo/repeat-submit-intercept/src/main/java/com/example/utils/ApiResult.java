package com.example.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@ToString
public class ApiResult {
    @Getter @Setter private Integer code;
    @Getter @Setter private String message;
    @Getter @Setter private Object data;
}
