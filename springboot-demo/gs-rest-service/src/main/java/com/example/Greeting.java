package com.example;


import lombok.*;

/**
 * @author xiaoming
 * @version 1.0
 * @date 2019-04-20 12:54
 **/
@Getter @Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Greeting {
     private long id;
     private String content;
}
