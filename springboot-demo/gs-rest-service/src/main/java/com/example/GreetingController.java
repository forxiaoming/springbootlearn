package com.example;

import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author xiaoming
 * @version 1.0
 * @date 2019-04-20 14:45
 **/
// @RestController使用 Spring4的注解, 将其标记为控制器, 而且返回的是一个域对象, 而不是视图
//它是@Controller和@ResponseBody拼凑在一起的
@RestController
public class GreetingController {
    private static final String template = "hello, %s";
    private static final AtomicLong counter = new AtomicLong();

    @RequestMapping(value = "/greeting")
    // @RequestParam 判定参数
    public Greeting greeting(@RequestParam(value = "name",defaultValue = "world") String name) {
        System.out.println("name: " + name);
        // Jackson 2 将对象转换为JSON
        return new Greeting(counter.incrementAndGet(), String.format(template, name));
    }
    /*
    @PathVariable能识别url中的模板,基于spring
     */

    @RequestMapping(value = "{name}/greeting")
    public Greeting greetingToName(@PathVariable("name") String name,
                                   @RequestParam(value = "name",defaultValue = "world") String name2) {

        System.out.println("ToName:" + name + " , Requestpatam: " +  name2);

        return new Greeting(counter.incrementAndGet(), String.format(template, name));
    }



}
