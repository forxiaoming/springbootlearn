package hello;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.CountDownLatch;

/**
 * https://spring.io/guides/gs/messaging-redis/
 * Redis 消息接收器
 * 这是一个简单的POJO
 * 定义了一个接收消息的方法
 */
public class Receiver {

    private static final Logger LOGGER = LoggerFactory.getLogger(Receiver.class);
    // 倒计时锁存器
    private CountDownLatch latch;

    @Autowired
    public Receiver(CountDownLatch latch) {
        this.latch = latch;
    }

    public void reveiverMessage(String message) {
        LOGGER.info("Reveived: {}" , message);
        latch.countDown();
    }
}
