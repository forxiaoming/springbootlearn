## springboot+mybatis

* PageHelper分页

## 声明式事务
只需要一个注解@Transactional 就可以了。
因为在springboot中已经默认对jpa、jdbc、mybatis开启了事事务，
引入它们依赖的时候，事物就默认开启。

只要使用常用ORM 即可, 默认开启
* 参考资料[Spring Boot教程第7篇：声明式事务](https://www.fangzhipeng.com/springboot/2017/05/07/sb7-tracstion.html)