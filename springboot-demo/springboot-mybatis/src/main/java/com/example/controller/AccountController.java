package com.example.controller;

import com.example.service.AccountService;
import com.example.vo.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author xiaoming
 * @version 1.0
 * @date 2019-04-26 23:53
 **/
@RestController
@RequestMapping(value = "/account")
public class AccountController {
    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public int updateByTransactional(@PathVariable(value = "id") int id,
                                      @RequestParam(value = "name") String name,
                                      @RequestParam(value = "money") double money) {
        Account account = new Account();
        account.setName(name);
        account.setMoney(money);
        account.setId(id);

        return accountService.updateTransfer(account);
    }
}
