package com.example.controller;

import com.example.service.UserService;
import com.example.vo.User;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * @author xiaoming
 * @version 1.0
 * @date 2019-04-23 18:18
 **/
@RestController
@RequestMapping(value = "/user")
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping(value = "", method = RequestMethod.POST)
    public int add(@RequestParam(value = "name") String name,
                      @RequestParam(value = "age") int age) {

        return  userService.add(name, age);

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public int update(@PathVariable(value = "id") int id,
                      @RequestParam(value = "name") String name,
                      @RequestParam(value = "age") int age) {
        return userService.update(name, age, id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public int delete(@PathVariable(value = "id") int id) {
        return userService.delete(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public User select(@PathVariable(value = "id") int id) {
        return userService.select(id);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public PageInfo<User> selectAll(@RequestParam(defaultValue = "1") int pageNo,
                                    @RequestParam(defaultValue = "10") int pageSize) {
        PageInfo<User> pageInfo = new PageInfo<>(userService.selectAll(pageNo, pageSize));
        return pageInfo;
        }

}
