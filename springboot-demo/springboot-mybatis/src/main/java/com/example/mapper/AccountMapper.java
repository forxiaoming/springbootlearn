package com.example.mapper;

/**
 * @author xiaoming
 * @version 1.0
 * @date 2019-04-26 23:27
 **/

import com.example.vo.Account;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface AccountMapper {
    @Update("update account set name=#{name}, money=#{money} where id = #{id}")
    public int updateTransfer(Account account);

}
