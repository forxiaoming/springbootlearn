package com.example.mapper;

import com.example.vo.User;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.*;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author xiaoming
 * @version 1.0
 * @date 2019-04-23 16:57
 **/
@Mapper
//@Repository
public interface UserMapper {
    @Insert("insert into user(name, age) values(#{name}, #{age})")
    int add(@Param("name") String name,@Param("age") int age);

    @Update("update user set name = #{name},age = #{age} where id= #{id}")
    int update(@Param("name") String name, @Param("age") int age, @Param("id") int id);

    @Delete("delete from user where id=#{id}")
    int delete(int id);
    @Transactional
    @Select("select id, name, age from user where id = #{id} ")
    User select(@Param("id") int id);

    @Select("select id,name,age from user ")
    //List<User> selectAll();
    // 返回分页信息
    Page<User> selectAll();

}
