package com.example.service;

import com.example.mapper.UserMapper;
import com.example.vo.User;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author xiaoming
 * @version 1.0
 * @date 2019-04-23 17:48
 **/
@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;

    public int add(String name, int age) {
        return userMapper.add(name, age);
    }

    public int update(String name, int age, int id) {
        return userMapper.update(name, age, id);
    }

    public int delete(int id) {
        return userMapper.delete(id);
    }

    public User select(int id) {
        return userMapper.select(id);
    }

    /**
     * PageHelper分页
     * @param pageNo
     * @param pageSize
     * @return Page<User>
     */
    public Page<User> selectAll(int pageNo, int pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        // 注意: PageHelper只会对其后第一个查询分页
        // userMapper.selectAll(); 这里会分页, 后面则不会分页
        return userMapper.selectAll();
    }
}
