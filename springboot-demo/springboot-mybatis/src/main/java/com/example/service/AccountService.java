package com.example.service;

import com.example.mapper.AccountMapper;
import com.example.vo.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author xiaoming
 * @version 1.0
 * @date 2019-04-26 23:39
 **/
@Service
public class AccountService {
    @Autowired
    private AccountMapper accountMapper;

    /**
     * 事务管理
     * 转账
     * 事务几个参数:
     *  transactionManager() 事务管理器
     *  isolatition 事务隔离级别
     *  propagation 事务传播行为
     */
    @Transactional
    public int updateTransfer(Account account) {
        // 改变account之前先改变account1
        Account account1 = new Account();
        account1.setId(1);
        account1.setName("小鸣-事务测试");
        account1.setMoney(33333.333);

        accountMapper.updateTransfer(account1);
        // exception
        int i = 1/0;

        return accountMapper.updateTransfer(account);
    }
}
