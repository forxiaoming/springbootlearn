package com.example.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * @author xiaoming
 * @version 1.0
 * @date 2019-04-26 23:23
 **/
public class Account {
    @Getter @Setter
    private int id;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private double money;
}
