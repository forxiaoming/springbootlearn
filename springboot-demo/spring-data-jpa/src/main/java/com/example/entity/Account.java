package com.example.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author xiaoming
 * @version 1.0
 * @date 2019-04-22 9:58
 **/
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table()
@Inheritance(strategy = InheritanceType.JOINED)
public class Account {
    @Id
    //@GeneratedValue
    // 解决【SpringBoot错误 ：Table 'XXX.hibernate_sequence' doesn't exist】
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter @Setter
    @Column(name = "id")
    private int id;

    @Getter @Setter
    @Column(nullable = true)
    private String name;

    @Getter @Setter
    private double money;


}
