package com.example.dao;

import com.example.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author xiaoming
 * @version 1.0
 * @date 2019-04-22 10:03
 **/
/*
Account 是实体类, 不是表名
Integer 是主键
 */
public interface AccountRepository extends JpaRepository<Account, Integer> {
    Account findById(int id);
}
