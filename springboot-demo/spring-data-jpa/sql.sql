-- auto-generated definition
create table account
(
  id    int          not null
    primary key,
  money double       not null,
  name  varchar(255) null
)
