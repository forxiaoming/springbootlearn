package com.example;

import lombok.*;

@ToString
@AllArgsConstructor
@NoArgsConstructor
public class User {

    @Getter @Setter
    private Long id;
    @Getter @Setter
    private String name;
    @Getter @Setter
    private Integer age;
}
