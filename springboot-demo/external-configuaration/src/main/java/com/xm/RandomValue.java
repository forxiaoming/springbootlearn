package com.xm;

import lombok.Getter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author xiaoming
 * @version 1.0
 * @date 2019-04-19 21:08
 **/
@Component
@ToString
public class RandomValue {
    @Value("${my.secret}")
    @Getter private String mySecret;
    @Value("${my.number}")
    @Getter private String myNumber;
    @Value("${my.bignumber}")
    @Getter private String myBignumber;
    @Value("${my.uuid}")
    @Getter private String myUUID;
}
