package com.xm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExternalConfiguarationApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExternalConfiguarationApplication.class, args);
    }

}
