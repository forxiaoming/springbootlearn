package com.xm;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

/**
 * 读取配置文件的值, 注入到bean
 * @author xiaoming
 * @version 1.0
 * @date 2019-04-19 19:12
 **/
@Component
@ToString
public class InjectedValueBean {
    /**
     * 系统属性值不可以直接注入到bean, 返回的是-1
     */
    @Value("${server.port}")
    @Getter private Integer port;
    /**
     *  自定义属性值可以直接注入到bean
     */
    @Value("${xm.name}")
    @Getter private String name;

    /**
     * xm.reference
     */
    @Value("${xm.reference}")
    @Getter private String reference;


}
