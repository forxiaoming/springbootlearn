package com.xm;

import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * 读取自定义配置文件
 * TODO: 这里报错: 提示Spring Boot Configuration Annotation Processor not fount in classP
 * 还不清楚为啥
 * @author xiaoming
 * @version 1.0
 * @date 2019-04-19 20:27
 **/
//@Configuration
//@PropertySource(value = "classpath ")
//@ConfigurationProperties(prefix = "my")
public class CustomPropertyFile {
    @Getter private String loading;
}
