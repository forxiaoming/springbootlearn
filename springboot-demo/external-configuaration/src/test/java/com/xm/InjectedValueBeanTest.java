package com.xm;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class InjectedValueBeanTest {

    @Autowired
    private InjectedValueBean injectedValueBean;

    @Autowired
    private RandomValue randomValue;

    @Test
    public void testInjectedValueBean() {
        System.out.println(injectedValueBean.toString());
    }

    @Test
    public void testCustomPropertyFile() {
    }

    @Test
    public void testRandomValue() {
        System.out.println(randomValue.getMyBignumber());
        System.out.println(randomValue.getMyNumber());
        System.out.println(randomValue.getMySecret());
        System.out.println(randomValue.getMyUUID());
    }
}