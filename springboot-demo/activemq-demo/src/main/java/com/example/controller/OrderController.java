package com.example.controller;

import com.example.activemq.service.ProducerService;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.jms.Destination;


@RestController
public class OrderController {
    @Autowired
    private ProducerService producerService;

    /**
     * 一对一 消息传递
     */
    @GetMapping("/order")
    private Object order(@RequestParam("msg") String msg) {
        Destination destination = new ActiveMQQueue("order.queue");

        // 点对点发送消息
        producerService.sendMsg(destination, msg);

        return String.format("Point to Point : %s", msg);
    }


    /**
     * 发布/订阅 消息传递
     */
    @GetMapping(value = "/topic")
    private Object topic(@RequestParam("msg") String msg ) {
        producerService.publish(msg);
        return "success";
    }
}
