package com.example.activemq.service.impl;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 *
 */
@Component
public class TopicConsumer {
    @JmsListener(destination = "demo.topic")
    public void receiver1(String text) {
        System.out.println(String.format("TopConsumer: receiver 1 : %s", text));
    }

    @JmsListener(destination = "demo.topic")
    public void receiver2(String text) {
        System.out.println(String.format("TopConsumer: receiverl 2: %s", text));
    }


    @JmsListener(destination = "demo.topic")
    public void receiver3(String text) {
        System.out.println( String.format("TopConsumer: receiver3: %s", text));
    }
}
