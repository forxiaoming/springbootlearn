package com.example.activemq.jms;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

/**
 * (点对点模型)
 * 消费者 (其实都是客户端)
 */
@Component
public class OrderConsumer {
    private static final Logger logger = Logger.getGlobal();

    /**
     * 监听指定消息队列
     */
    @JmsListener(destination = "order.queue")
    public void receiveQueue(String text) {
        logger.info(String.format("OrderConsumer 收到的报文: %s ", text));
    }
}
