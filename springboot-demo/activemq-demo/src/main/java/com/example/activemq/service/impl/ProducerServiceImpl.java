package com.example.activemq.service.impl;

import com.example.activemq.service.ProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Service;

import javax.jms.Destination;
import javax.jms.Topic;

/**
 * 实现消息的发送
 */
@Service
public class ProducerServiceImpl implements ProducerService {

    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;
    @Autowired
    private Topic topic;


    /**
     * 点对点发送消息
     * @param destination
     * @param message
     */
    @Override
    public void sendMsg(Destination destination, String message) {
        jmsMessagingTemplate.convertAndSend(destination, message);

    }

    /**
     * 消息发布者 定义
     * 发布/订阅模式, 消息推送
     */
    @Override
    public void publish(String msg) {
        jmsMessagingTemplate.convertAndSend(topic, msg);
    }
}
