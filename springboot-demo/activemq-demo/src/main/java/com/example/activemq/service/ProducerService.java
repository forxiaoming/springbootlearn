package com.example.activemq.service;

import javax.jms.Destination;

/**
 * 消息生产者
 * 属于客户端
 */
public interface ProducerService {
    /**
     * 指定消息队列发送
     * @param destination
     * @param message
     */
    void sendMsg(Destination destination, final String message);


    /**
     * 消息发布者
     */
    void publish(String  msg);
}
