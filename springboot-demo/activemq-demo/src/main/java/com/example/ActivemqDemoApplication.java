package com.example;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.ConnectionFactory;
import javax.jms.Topic;

@SpringBootApplication
@EnableJms // 在 SpringBoot 中开启 JMS
public class ActivemqDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActivemqDemoApplication.class, args);
	}

	@Autowired
	private Environment environment;

	@Bean
	public ConnectionFactory connectionFactory() {
		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
		connectionFactory.setBrokerURL(environment.getProperty("spring.activemq.broker-url"));
		connectionFactory.setUserName(environment.getProperty("spring.activemq.user"));
		connectionFactory.setPassword(environment.getProperty("spring.activemq.password"));
		return connectionFactory;
	}

	@Bean
	public JmsTemplate genJmsTemplate() {
		return new JmsTemplate(connectionFactory());

	}

	@Bean
	public JmsMessagingTemplate jmsMessageTemplate() {
		return new JmsMessagingTemplate(connectionFactory());
	}


	// Spring  管理主题对象
	@Bean
	public Topic topic() {
		return new ActiveMQTopic("demo.topic");
	}
}
