## 数据验证
### JSR-303 规范

1. 系统自带的注解
* ``javax.validation``
* ```org.hibernate.validator```
* 基于正则的表达式 ```@Pattern```

2. 自定义注解
* 注解实现```ConstraintValidator ```接口

示例: [./src/main/java/com/demo/define/validation](./src/main/java/com/demo/define/validation)

3. 分组验证

如何用一个验证类实现多个接口之间不同规则的验证 ?

通过设置验证注解的 ```Class<?>[] groups() default {};```属性

空接口分组类 : [./src/main/java/com/demo/validation/group/Groups.java](./src/main/java/com/demo/validation/group/Groups.java)

```java
// 指定分组
    @GetMapping("/testGroupsUpdate")
    public String testGroupsUpdate(@Validated(value = Groups.Update.class) BookGroup bookGroup) {
        return bookGroup.toString();
    }
```
## 参考

* [java7 api](https://docs.oracle.com/javaee/7/api/javax/validation/package-summary.html)
* [谭亚峰: 一起来学SpringBoot | 第二十一篇：轻松搞定数据验证](https://blog.battcn.com/tags/Validate/)
* [Bean Validation 技术规范特性概述]()