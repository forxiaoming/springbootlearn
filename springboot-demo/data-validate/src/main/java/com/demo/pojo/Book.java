package com.demo.pojo;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class Book{
    @Setter @Getter
    private Integer id;

    @Setter @Getter
    @NotNull(message = "name 不能为空")
    @Length(min = 2, max = 10, message = "name 长度必须在 {min}和{max}之间")
    private String name;

    @Setter @Getter
    @NotNull(message = "price 不能为空")
    @DecimalMin(value = "0.1", message = "price 不能低于{value}")
    private BigDecimal price;

}
