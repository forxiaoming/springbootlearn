package com.demo.pojo;

import com.demo.validation.group.Groups;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@ToString
public class BookGroup {
    @Setter
    @Getter
    @NotNull(message = "id 不能为空", groups = Groups.Update.class)
    private Integer id;

    @Setter @Getter
    @NotNull(message = "name 不能为空", groups = Groups.Default.class)
    @Length(min = 2, max = 10, message = "name 长度必须在 {min}和{max}之间")
    private String name;

    @Setter @Getter
    @NotNull(message = "price 不能为空", groups = Groups.Default.class)
    @DecimalMin(value = "0.1", message = "price 不能低于{value}")
    private BigDecimal price;


}
