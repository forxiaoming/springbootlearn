package com.demo.controller;

import com.demo.pojo.Book;
import com.demo.pojo.BookGroup;
import com.demo.validation.define.DateTime;
import com.demo.validation.group.Groups;
import org.hibernate.validator.constraints.Length;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;

@Validated
@RestController
public class ValidateController {
    @GetMapping("/test1")
    public String test(@NotBlank(message = "name 不能为空")
                       @Length(min = 2, max = 10, message = "name 长度必须在 {min} - {max} 之间")
                               String name) {

        return "success > name : " + name;
    }

    @GetMapping("/test2")
    public Book test2(@Validated Book book) {

        return book;
    }

    @GetMapping("/testDefined")
    public String testDefined(@DateTime(message = "输入日期的格式错误, 正确格式: {format}", format = "yyyy-MM-dd HH:mm") String date) {

        return "success: " + date;
    }

    @GetMapping("/testGroupsInsert")
    public String testGroupsInsert(@Validated(value = Groups.Default.class) BookGroup bookGroup) {
        return bookGroup.toString();
    }

    @GetMapping("/testGroupsUpdate")
    public String testGroupsUpdate(@Validated(value = Groups.Update.class) BookGroup bookGroup) {
        return bookGroup.toString();
    }
}
