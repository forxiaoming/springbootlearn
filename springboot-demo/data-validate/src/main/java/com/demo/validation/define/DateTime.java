package com.demo.validation.define;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({FIELD, PARAMETER })
@Retention(RUNTIME)
// 指定校验器
@Constraint(validatedBy = DateTimeValidator.class)
// 自定义注解
public @interface DateTime {
    // 验证失败提示的消息内容
    String message() default "格式错误";

    String format() default "yyyy-MM-dd";
    // 为约束指定验证组
    Class<?>[] groups() default {};

    Class< ? extends Payload>[] payload() default {};

}
