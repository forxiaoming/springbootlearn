package com.demo.validation.define;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;

// 校验器实现
public class DateTimeValidator implements ConstraintValidator<DateTime, String> {
   private DateTime dateTime;
   // 主要用于初始化，它可以获得当前注解的所有属性
   @Override
   public void initialize(DateTime _datetime) {
      this.dateTime = _datetime;
   }

   // 进行约束验证的主体方法，其中 value 就是验证参数的具体实例，context 代表约束执行的上下文环境。
   @Override
   public boolean isValid(String value, ConstraintValidatorContext context) {
      if (value == null) {
         return true;
      }

      String format = dateTime.format();

      if (value.length() != format.length()) {
         return false;
      }
      SimpleDateFormat sdf = new SimpleDateFormat(format);

      try {
         sdf.parse(value);
      } catch (ParseException e) {
         return false;
      }

      return true;
   }
}
