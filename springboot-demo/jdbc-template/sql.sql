-- auto-generated definition
create table user
(
  id   int auto_increment
    primary key,
  name varchar(50) not null,
  age  int(5)      not null
);
