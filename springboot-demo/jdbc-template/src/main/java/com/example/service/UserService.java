package com.example.service;

import com.example.vo.User;

import java.util.List;

/**
 * @author xiaoming
 * @version 1.0
 * @date 2019-04-21 10:26
 **/
public interface UserService {
    /**
     * 新增用户
     */
    int add(User user);
    /**
     * 根据ID删除用户
     */
    int delById(int id);
    /**
     * 修改用户
     */
    int update(User user);

    /**
     * 根据id查询用户
     */
    User findById(int id);

    /**
     * 分页查询用户
     * TODO JdbcTemplage分页查询
     */
    List<User> findAllByPage();
}
