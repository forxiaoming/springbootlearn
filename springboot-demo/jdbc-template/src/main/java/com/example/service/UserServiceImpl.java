package com.example.service;

import com.example.dao.UserDao;
import com.example.vo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author xiaoming
 * @version 1.0
 * @date 2019-04-21 14:27
 **/
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;
    @Override
    public int add(User user) {
        return userDao.add(user);
    }

    @Override
    public int delById(int id) {
        return userDao.delById(id);
    }

    @Override
    public int update(User user) {
        return userDao.update(user);
    }

    @Override
    public User findById(int id) {
        return userDao.findById(id);
    }

    @Override
    public List<User> findAllByPage() {
        return userDao.findAllByPage();
    }
}
