package com.example.vo;

import lombok.*;

/**
 * @author xiaoming
 * @version 1.0
 * @date 2019-04-21 10:35
 **/
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class User {
    @Setter @Getter private int id;
    @Setter @Getter private String name;
    @Setter @Getter private int age;

}
