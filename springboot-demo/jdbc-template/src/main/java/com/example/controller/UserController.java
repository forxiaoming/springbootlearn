package com.example.controller;

import com.example.service.UserService;
import com.example.vo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author xiaoming
 * @version 1.0
 * @date 2019-04-21 10:49
 **/
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public int add(@RequestParam(value = "name", required = true) String name,
                   @RequestParam(value = "age", required = true) int age) {

        User user = new User();
        user.setName(name);
        user.setAge(age);
        return userService.add(user);
    }

    /**
     * 根据id删除user
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}/del", method = RequestMethod.DELETE)
    public int delById(@PathVariable(value = "id", required = true) int id  ) {
        return userService.delById(id);
    }
    @RequestMapping(value = "/{id}/update", method = RequestMethod.POST)
    public int update(@PathVariable(value = "id", required = true) int id,
                      @RequestParam(value = "name") String name,
                      @RequestParam(value = "age") int age) {
        // 应该先查询有没有再修改
        User user = new User(id, name, age);
        return userService.update(user);
    }

    @RequestMapping(value = "/{id}/query",method = RequestMethod.GET)
    public User findById(@PathVariable("id") int id) {
        return userService.findById(id);
    }

}
