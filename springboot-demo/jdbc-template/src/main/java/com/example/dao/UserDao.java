package com.example.dao;

import com.example.vo.User;

import java.util.List;

/**
 * @author xiaoming
 * @version 1.0
 * @date 2019-04-21 10:33
 **/
public interface UserDao {
    /**
     * 新增用户
     */
    int add(User user);
    /**
     * 根据ID删除用户
     */
    int delById(int id);
    /**
     * 修改用户
     */
    int update(User user);

    /**
     * 根据id查询用户
     */
    User findById(int id);

    /**
     * 分页查询
     * TODO JdbcTemplage
     */
    List<User> findAllByPage();

}
