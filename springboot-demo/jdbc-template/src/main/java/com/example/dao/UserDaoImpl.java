package com.example.dao;

import com.example.vo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

/**
 * @author xiaoming
 * @version 1.0
 * @date 2019-04-21 10:47
 **/
@Repository // 声明Dao
public class UserDaoImpl implements UserDao{
    @Autowired // 注入
    private JdbcTemplate jdbcTemplate;

    /**
     * 实现插入一条数据并返回主键
     * @param user
     * @return
     */
    @Override
    public int add(User user) {
//        return jdbcTemplate.update("INSERT INTO user(name, age) VALUES (?,?)",
//                user.getName(), user.getAge());

        KeyHolder keyHolder = new GeneratedKeyHolder();
        // lambda 优化前
/*        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                // jdbc, 注意指定 Statement.RETURN_GENERATED_KEYS
                PreparedStatement preparedStatement =
                        connection.prepareStatement("INSERT INTO user(name, age) VALUES (?,?)",
                                Statement.RETURN_GENERATED_KEYS);
                preparedStatement.setString(1, user.getName());
                preparedStatement.setInt(2, user.getAge());
                //preparedStatement.execute();
                return preparedStatement;
            }
        }, keyHolder);*/

        // lambda 优化后:
//        PreparedStatementCreator pre = connection -> {
//            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO user(name, age) VALUES (?,?)",
//                    Statement.RETURN_GENERATED_KEYS);
//            preparedStatement.setString(1, user.getName());
//            preparedStatement.setInt(2, user.getAge());
//            return preparedStatement;
//        };

        jdbcTemplate.update(connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO user(name, age) VALUES (?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, user.getName());
            preparedStatement.setInt(2, user.getAge());
            return preparedStatement;
        } , keyHolder);

        return keyHolder.getKey().intValue();
    }

    @Override
    public int delById(int id) {
        return jdbcTemplate.update("DELETE FROM user WHERE id = ?",
                id);
    }

    @Override
    public int update(User user) {
        return jdbcTemplate.update("UPDATE user SET name = ?, age=? WHERE id=?",
                user.getName(), user.getAge(), user.getId());
    }

    @Override
    public User findById(int id) {
        List<User> lists = jdbcTemplate.query("SELECT * FROM user WHERE id = ?",
                new Object[]{id}, new BeanPropertyRowMapper(User.class));
        if (lists.isEmpty()) {
            return null;
        } else {
            User user = lists.get(0);
            return user;
        }
    }

    @Override
    public List<User> findAllByPage() {
        return null;
    }

}
