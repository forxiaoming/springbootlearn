package com.ming;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ImportResource;

/**
 * 主程序类
 */
// 直接 引入 spring 配置文件
//@ImportResource("classpath:beans.xml")
@SpringBootApplication
@EnableConfigurationProperties
public class HelloWorldMainApplication {
    public static void main(String[] args) {
        // spring 应用启动起来
        SpringApplication.run(HelloWorldMainApplication.class, args);
    }
}
