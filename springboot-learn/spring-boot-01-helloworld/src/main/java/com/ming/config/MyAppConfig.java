package com.ming.config;

import com.ming.service.HelloService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * 配置类,  替代 Spring 配置文件 ( <bean> 标签)
 * Springboot 推荐方式
 */
// 声明配置类
@Configuration
//@ImportResource("classpath:beans.xml")
public class MyAppConfig {
    @Bean
    public HelloService helloService(){
        System.out.println("配置类 @Bean 给容器中添加组件...");
        return new HelloService();
    };
}
