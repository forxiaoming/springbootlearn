package com.ming.bean;


import com.ming.HelloWorldMainApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * SpringBoot 单元测试
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HelloWorldMainApplication.class)
public class PersonTest {

    @Autowired
    Person person;

    @Autowired
    ValuePerson valuePerson;
    @Autowired
    PropertyPerson propertyPerson;

    @Autowired
    ApplicationContext ioc;

    @Test
    public void contextLoads() {
        System.out.println(person);
    }

    @Test
    public void valuePerson() {
        System.out.println(valuePerson);
    }

    @Test
    public void propertyPerson() {
        System.out.println(propertyPerson);
    }

    @Test
    public void testHelloService() {
        boolean containsBean = ioc.containsBean("helloService");
        System.out.println("contains HelloService : " + containsBean);
    }

}