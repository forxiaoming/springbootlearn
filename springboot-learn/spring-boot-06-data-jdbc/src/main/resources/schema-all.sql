use jdbc;


create table if not exists `user`
(
    `id`   int unsigned auto_increment,
    `name` varchar(100) not null,
    `age`  int,
    primary key (`id`)
) engine = InnoDB
  default charset = utf8;
