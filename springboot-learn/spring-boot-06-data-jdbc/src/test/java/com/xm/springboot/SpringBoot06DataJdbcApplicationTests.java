package com.xm.springboot;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sound.midi.Soundbank;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

@SpringBootTest
class SpringBoot06DataJdbcApplicationTests {

    /**
     * 注入数据源
     */
    @Autowired
    DataSource dataSource;

    @Test
    void contextLoads() throws SQLException {

        // 获取数据源
        System.out.println("数据源类型: " + dataSource.getClass());
        // 获取连接
        Connection connection = dataSource.getConnection();

        System.out.println("连接: " + connection);

        connection.close();
    }

}
