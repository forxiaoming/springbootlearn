package com.xm.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

/*

	@Bean
	public ViewResolver myViewResolver() {
		return new MyViewResolver();
	}
*/

	/**
	 * 自定义一个视图解析器
	 * 实现 ViewResolver
	 */
/*
	private static class MyViewResolver implements ViewResolver {
	    // 如何看自 定义视图解析器起作用
        // 1. DispatcherServlet.doDispatch() 打断点
        // 2. 随意访问一个请求

		@Override
		public View resolveViewName(String viewName, Locale locale) throws Exception {
			return null;
		}
	}
*/

}
