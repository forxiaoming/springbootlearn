package com.xm.springboot.servlet.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * 自定义 listener
 *
 * @author xiaoming
 * @date 2020-10-27 21:27
 */
public class MyListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println(" contextInitialized .... 服务器初始化");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println(" contextDestroyed .... 当前 web 项目销毁");

    }
}
