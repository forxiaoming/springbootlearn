package com.xm.springboot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @author xiaoming
 * @date 2020-08-08 14:44
 */
@Controller
public class LoginController {


    @PostMapping(path = "/user/login")
    public String login(@RequestParam("username") String username,
                        @RequestParam("password") String password,
                        Map<String ,Object> map,
                        HttpSession session) {

        // 简单验证用户密码
        if (!StringUtils.isEmpty(username) && "345".equals(password)) {

//            map.put("msg", "登录成功");
            // 登录成功, 防止重复提交,可以重定向到主页
            session.setAttribute("loginUser", username);
            return "redirect:/main.html";
        } else {
            map.put("msg", "用户名密码错误");
            return "login";
        }

    }
}
