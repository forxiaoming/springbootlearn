package com.xm.springboot.controller;

import com.xm.springboot.exception.UserNotExistException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author xiaoming
 * @date 2020-05-14 6:57
 */
@Controller
public class HelloController {
/*
    // 不推荐的使用方法, 推荐
    @RequestMapping({"/", "/index.html"})
    public String index() {
        return "index"; // templates/index.html
    }
*/

    static final String NULL_CHARACTER = "null";
    
    @ResponseBody
    @RequestMapping("/hello")
    public String hello(@RequestParam("user") String user) {
        if ( NULL_CHARACTER.equals(user)) {
            throw new UserNotExistException();
        }
        return "hello springboot";

    }

}
