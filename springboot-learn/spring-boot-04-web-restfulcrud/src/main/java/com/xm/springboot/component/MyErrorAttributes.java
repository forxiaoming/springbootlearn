package com.xm.springboot.component;

import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;

import java.util.Map;

/**
 * 给容器中加入我们自己定义的 ErrorAttributes
 */
@Component
public class MyErrorAttributes  extends DefaultErrorAttributes {
    @Override
    public Map<String, Object> getErrorAttributes(WebRequest webRequest, boolean includeStackTrace) {
        Map<String, Object> map = super.getErrorAttributes(webRequest, includeStackTrace);
        map.put("company", "mingming");
        // 获取来自异常处理器的数据
        Map<String, Object> exp = (Map<String, Object>) webRequest.getAttribute("exp", RequestAttributes.SCOPE_REQUEST);
        map.put("ext", exp);
        return map;
    }
}
