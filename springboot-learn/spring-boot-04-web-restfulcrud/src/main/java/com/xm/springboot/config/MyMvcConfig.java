package com.xm.springboot.config;

import com.xm.springboot.component.MyLocaleResolver;
import com.xm.springboot.filter.MyFilter;
import com.xm.springboot.servlet.MyServlet;
import com.xm.springboot.servlet.listener.MyListener;
import org.springframework.boot.web.server.ConfigurableWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;

/**
 *  实现 WebMvcConfigurer类来扩展 SpringMVC 功能
 * ( 1.x 版本中使用的是 `@Deprecated WebMvcConfigurerAdapter`)
 *
 * 参考博客 [Spring Boot配置接口 WebMvcConfigurer](https://blog.csdn.net/fmwind/article/details/81235401)
 */
// @EnableWebMvc  开启后不能直接访问 URL  `/` 路径  `public/index.html`页面
@Configuration
public class MyMvcConfig implements WebMvcConfigurer {
    /**
     * 注册三大组件
     * 1. 自定义 Servlet
     */
    @Bean
    public ServletRegistrationBean mySevlet() {
        ServletRegistrationBean<MyServlet> registration = new ServletRegistrationBean(new MyServlet(), "/myServlet");
        registration.setLoadOnStartup(1);

        return registration;
    }
    /**
     * 注册三大组件
     * 2. 自定义 Filter
     */
    @Bean
    public FilterRegistrationBean myFilter() {
        FilterRegistrationBean filter = new FilterRegistrationBean();
        filter.setFilter(new MyFilter());
        filter.setUrlPatterns(Arrays.asList("/hello", "/myServlet"));
        return filter;
    }

    /**
     * 注册三大组件
     * 3. 自定义 listener
     */
    @Bean
    public ServletListenerRegistrationBean servletListener() {
        ServletListenerRegistrationBean<MyListener> listener = new ServletListenerRegistrationBean(new MyListener());
        return listener;
    }
    /**
     * 定制嵌入式 Servlet 容器
     *
     */
    @Bean
    public WebServerFactoryCustomizer<ConfigurableWebServerFactory> webServerFactoryCustomizer() {
        return new WebServerFactoryCustomizer<ConfigurableWebServerFactory>() {
            @Override
            public void customize(ConfigurableWebServerFactory factory) {
                // 覆盖了 配置文件中的 servlet.port
//                factory.setPort(8092);
            }
        };
    }

    /**
     1. 引入 thymeleaf 依赖
     2. viewName 位于 templates 目录下
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/hellomvc").setViewName("success");
    }

    @Bean
    public WebMvcConfigurer webMvcConfigurer() {
        WebMvcConfigurer  configurer = new WebMvcConfigurer(){
            /**
             * 配置视图解析器
             * @param registry
             */
            @Override
            public void addViewControllers(ViewControllerRegistry registry) {
                // 添加默认首页, 避免写电多余的 Controller类
                registry.addViewController("/").setViewName("login");
                registry.addViewController("/index.html").setViewName("login");
                registry.addViewController("/main.html").setViewName("dashboard");
            }


            /**
             * 注册拦截器
             * @param registry
             */
            @Override
            public void addInterceptors(InterceptorRegistry registry) {
                /**
                 * 临时屏蔽以测试错误处理
                 * xiaoming
                 * 2020-08-30 3:25
                 */

                /**
                 * 静态资源:
                 * SpringBoot 已经做好了静态资源映射 (v1.0)
                 * v2.0之后, 自定义拦截器后需要手动排除
                 */
               /* registry.addInterceptor(new MyLoginHandlerInterceptor())
                        .addPathPatterns("/**")
                        // 排除请求
                        .excludePathPatterns("/index.html", "/", "/user/login")
                        // v2.0 需要手动排除静态资源映射
                        .excludePathPatterns("/asserts/**")
                        .excludePathPatterns("/webjars/**");*/
            }
        };

        return configurer;

    }

    /**
     * 将自定义的 LocalResolver 组件注册到容器中
     */
    @Bean
    public LocaleResolver localResolver() {
        return new MyLocaleResolver();
    }


}
