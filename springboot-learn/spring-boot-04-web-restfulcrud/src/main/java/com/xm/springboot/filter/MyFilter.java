package com.xm.springboot.filter;

import javax.servlet.*;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * @author xiaoming
 * @date 2020-10-25 22:59
 */
public class MyFilter implements Filter {
    private static final Logger logger = Logger.getGlobal();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        logger.info("My Filter");

        filterChain.doFilter( servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
