package com.demo;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DemoApplicationTests {
//	Logger logger = LoggerFactory.getLogger(DemoApplicationTests.class.getSimpleName());
	Logger logger = LoggerFactory.getLogger("Logger name");

	@Test
	void contextLoads() {
		logger.trace("这是 trace()");
		logger.debug("这是 debug()");
		logger.info("这是 info()");
		logger.warn("这是 warn()");
		logger.error("这是 error()");
	}

}
