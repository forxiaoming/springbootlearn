# 一、入门

> 代码: spring-boot-01-helloworld
> 学习视频中使用 springboot v1.5, 这里实操使用 v2.x

## 1  简化部署

maven-plug

## 2 HelloWorld 探究

### 1. pom文件探究

#### 1.1  starter的父项目

```
<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.2.2.RELEASE</version>
    <relativePath/> <!-- lookup parent from repository -->
</parent>
```

* SpringBoot 版本仲裁中心: 我们导入依赖默认不需要写版本(dependencies中没有管理的依赖需要声明)

```
<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-dependencies</artifactId>
    <version>2.2.2.RELEASE</version>
    <relativePath>../../spring-boot-dependencies</relativePath>
</parent>
```
#### 1.2  导入依赖
```
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
</dependencies>
```

* spring-boot-starter-xxx : 场景启动器, 帮我们导入了模块运行所需依赖的组件;
* SpringBoot将所有的功能都抽取出来, 做成一个个的starters(启动器);

### 2. 主程序类

#### 2.1 `@SpringBootApplication`

* `@EnableAutoConfiguration`:  告诉SpringBoot开启自动配置

  * ```java
    @AutoConfigurationPackage
    @Import({AutoConfigurationImportSelector.class})
    public @interface EnableAutoConfiguration {}
    ```
  
* `@AutoConfigurationPackage`: 自动配置的包

  * ```java
    @Import({Registrar.class})
    public @interface AutoConfigurationPackage {
    }
    ```
    
  * `@import`, Spring底层注解, 给容器导入一个组件;

  * `Registrar.class` 中获取了`@SpringBootApplication`注解的类所在的包下面的所有组件扫描到Spring组件.

* `@Import(){AutoConfigurationImportSelector.class)}`  开启自动导包选择器

  * 给容器导入组件
  * `String[] selectImports()`将所有需要导入的组件以全类名的方式返回, 这些组件就会被添加到容器中;
  * 有了自动配置类, 就免去了我们手写编写配置注入功能组件的工作:
  * 从```C:\DevTool\.m2\repository\org\springframework\boot\spring-boot-autoconfigure\2.2.2.RELEASE\spring-boot-autoconfigure-2.2.2.RELEASE.jar!\META-INF\spring.factories```中读取自动配置的类

* `@SpringBootConfiguration` --> `@Configuration (spring)`


# 二、配置文件

## 1 配置文件

### 1.1 全局的配置文件, 文件名是固定的

* *application.properties
* *application.yml
* 作用: 修改SpringBoot自动配置的默认值.

## 2 YAML

* 以**数据为中心**, 所以比JSON, XML更适合做配置文件

### 2.1 基本用法

* 严格使用 **空格** 缩进, key: value之间也要空格;
* 不允许`tab`
* 大小写敏感;
* 缩进表示层级关系;

### 2.2 值的写法

#### 2.2.1  字面量: 普通值 ( 数组, 字符串, 布尔)

* 不用加 单引号(转义特殊字符) / 双引号(不转义特殊字符)

```yaml
	k: v
```



#### 2.2.2 对象、Map

```yaml
friends:
  lastName: zhangsan
  age: 20
  
# 行内写法
friends: {lastName: zhoangsan,age: 18}
```



#### 2.2.3 数组(List, Set)

```yaml
pets:
  - cat
  - dog
  - pig
  
# 行内写法
pets: [cat,dog,pig]
```

## 3. 配置文件值的注入 获取



### 3.1 与 @Bean 结合为属性赋值

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-configuration-processor</artifactId>
    <optional>true</optional>
</dependency>

<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-test</artifactId>
</dependency>
```

```java
@Component
@ConfigurationProperties("person")
public class Person {
...
}

@SpringBootApplication
@EnableConfigurationProperties
public class HelloWorldMainApplication {
...
}
```

* @ConfigurationProperties
  * 与 @Bean 结合为属性赋值
  * 与 @PropertySource ( only properties 文件) 结合读取指定文件

### 3.2  properties 配置文件编码问题

* idea 设置 Editor > File Encodigns

![ idea Editor > File Encodigns](./img/01-editor-file-encodigns.png)

## 4. @ConfigurationProperties 与 @Value



|                  | @ConfigurationProperties | @Value   |
| ---------------- | ------------------------ | -------- |
| 功能              | 批量注入配置文件中的属性 | 单独指定 |
| 松散语法          | 支持                     | 不支持   |
| SpEL             | 不支持                   | 支持     |
| JSR303 数据校验   | 支持                      | 不支持 |
| 复制类型封装       | 支持 | 不支持 |

* 配置文件 yaml 和 properties 它们都能获取到
* 获取配置文件某项值, 用 `@Value`
* 映射 javaBean, 用 `@ConfigurationProperties`

## 5.@ConfigurationProperties Validation  (JSR303 数据校验)



## 6. @PropertySource  & ImportResource 

### 6.1  @PropertySource 

​	*  读取指定文件

### 6.2 @ImportResource  (给容器添加组件)

* 导入 Spring 的配置文件, 让配置文件里面的内容生效
* 编写 spring bean xml 配置文件, 然后在配置类中 使用注解引入实现

### 6.3 SpringBoot 推荐方式 (给容器添加组件)

* 新建配置类  -- 代替 --> Spring 配置文件
* 使用 @Bean  给容器添加组件

## 7. 配置文件占位符

* RandomValuePropertySource: 配置文件中可以使用随机数
* 属性配置占位符 `${}` 读取已配置的属性

##  8. Profile 多环境支持

### 8.1  多 profile支持
* 格式: `application-{profile}.properties`

### 8.2  多profile文档块模式

### 8.3 激活模式

* 命令行 --spring.profiles.active=dev
* 配置文件 spring.profiles.active=dev
* jvm 参数

## 9. 配置文件的加载位置

* 启动扫描一下位置的 application.properties / application.yml 作为默认配置文件
* 按优先级从高到低加载,  高优先级覆盖低优先级
  * `file:/config/`  
  * `fiel:./`
  * `classpath:/config/`
  * `classpath:/`
* 由jar包外向jar包内进行寻找，**优先加载带`profile`的**，再加载不带`profile`的。
* 配置互补
* `spring.config.location` 改变默认配置  

## 10. 外部配置加载顺序

* 官方文档 - 外部化配置: [Spring Boot Features -> Externalized Configuration -> ](https://docs.spring.io/spring-boot/docs/2.2.6.RELEASE/reference/html/spring-boot-features.html#boot-features-external-config)
* 官方文档提供了 17种配置方式
* 多个配置文件, 命令行修改

## 11. 自动配置原理[重要]

>  模块代码 : spring-boot-02-config-autoconfig
>  springboot v2.1.13

* 我们能直接写哪些配置???
  * 官方文档-配置文件能配置的属性参考(通用应用程序属性) : [Common Application properties](https://docs.spring.io/spring-boot/docs/2.2.6.RELEASE/reference/html/appendix-application-properties.html#common-application-properties)
  * ![image-20200417204822549](.\img\image-20200417204822549.png)
* 为什么我们能配置这些自动配置???
  * 所以我们需要来了解自动配置原理

### 11.1 自动配置原理 

* 分析 Springboot自动配置原理.drawio  
*  见云盘配置图

### 11.2 自动配置原理实例 (HttpEncodingAutoConfiguration)

(每个自动配置类, 进行自动配置功能)

```java
@Configuration // 表示这是一个配置类, 可以给容器添加组件 (@Brean)
@EnableConfigurationProperties({HttpProperties.class}) // 启用指定类的 @ConfigurationProperties 功能;
// 可以将配置文件中对应的值和 HttpProperties 绑定起来; 并把 HttpProperties 加入 spring ioc中
@ConditionalOnWebApplication(
    type = Type.SERVLET
)// Spring 底层 @Conditional 注解, 根据不同条件, 如果满足指定条件, 整个配置类里面的配置就会生效;
// 判断当前应用是否为web应用, 如果是, 当前配置类生效
@ConditionalOnClass({CharacterEncodingFilter.class}) // 判断当前项目有没有这个类
// CharacterEncodingFilter: SpringMVC 乱码过滤器
@ConditionalOnProperty(
    prefix = "spring.http.encoding",
    value = {"enabled"},
    matchIfMissing = true
) // 判断配置文件中是否存某个配置 "spring.http.encoding", `matchIfMissing = true` 如果不存在, 判断也是处理
public class HttpEncodingAutoConfiguration {
    private final Encoding properties;
    // 只有一个有参构造器的情况下, 参数的值就会从容器中拿
    public HttpEncodingAutoConfiguration(HttpProperties properties) {
        this.properties = properties.getEncoding();
    }
    
    @Bean  // 给容器添加一个组件
    @ConditionalOnMissingBean
    public CharacterEncodingFilter characterEncodingFilter() {}
}


// 已经和springboot配置文件映射了
// 从配置文件中获取指定的值和Bean的属性进行绑定
@ConfigurationProperties(
    prefix = "spring.http"
)
public class HttpProperties {}

```

根据当前不同的条件判断 决定这个配置类是否失效?

一旦这个配置类生效, 这个配置类就会个容器中添加各种组件; 这些组件的属性是从对应的 xxProperties 类中获取;

这些类里面的每个属性又是和配置文件绑定的;

## 12.  @Conditional & 自动配置报告

* 自动配置类必须在一定条件下才能生效.
* 如何查看哪些自动配置类生效 ?
    ```properties
    # 开启springboot的debug模式
    debug=true
    # 控制台会打印启用的自动配置类和没有启用没有匹配成功的类
    ```



# 三、日志框架~

## 1.简介

* 日志门面: 日志的一个抽象层
* 日志实现

| 日志门面                                                     | 日志实现                                  |
| ------------------------------------------------------------ | ----------------------------------------- |
| JCL (比较老了) , SLF4j (Simple Logging Facade for java) , jboss-logging | Log4j,  Logback , JUL (java.util.logging) |

* Springboot :  SLF4j  + Logback
* Spring : 默认 JCL

## 2. SLF4j 使用

### 2. 1 如何在系统中使用 SLF4j

* 开发时, 日志记录方法的调用不应该直接调用实现类, 而是调用日志抽象类

* 导入 slf4j 的 jar和 logback 的实现 jar

* ````java
  import org.slf4j.Logger;
  import org.slf4j.LoggerFactory;
  
  public class HelloWorld {
    public static void main(String[] args) {
      Logger logger = LoggerFactory.getLogger(HelloWorld.class);
      logger.info("Hello World");
    }
  }
  ````
  
* 具体的绑定
  ![具体的绑定](http://www.slf4j.org/images/concrete-bindings.png)

  * 每个日志的实现框架都有自己的配置文件, 使用 slf4j 后, 配置文件使用 实现框架本身的配置文件.

### 2.2 遗留问题 Legacy APIs (统一日志框架)

a (slf4j + logback) : Spring ( commons-logging), Hibernate(jbos-logging), MyBatis..
* 如何让不同的框架和我们的项目一起实现统一的日志输出(slf4j) ?
	* 1. 排除系统中其他日志框架;
	* 2. 用中间包来替换原有的日志框架;
	* 3. 导入 slf4j 其他的实现;

* [](http://www.slf4j.org/legacy.html)
* ![legacy APIs.](http://www.slf4j.org/images/legacy.png)

### 2.3 Springboot 日志关系

> 模块代码: spring-boot-03-logger
> SpringBoot   2.2.7.RELEASE

```xml
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter</artifactId>
</dependency>
```
SpringBoot 使用它来做日志功能 :
```xml
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-logging</artifactId>
</dependency>
```

* spring-boot-starter-logging  2.2.7版本 底层依赖
  ![spring-boot-starter-logging](./img/03_001_springboot_log.png)
  
  ![spring-boot-starter-logging](./img/03_002_springboot_log.png)
  
* SpringBoot 1.5.x 老版本 底层依赖关系
  ![spring-boot-starter-logging](./img/03_003_springboot_log.png)

* 总结  (基于 1.5.x 版本)

* ( 1.5.x 版本, 2.2.7版本发生了不同, 从什么可以看出, 2.x 没有 `xx-over-slf4j` 中间替换包)

  * 1. SpringBoot 底层使用 slf4j + logback 方式记录日志

    2. SpringBoot 把其他框架替换为 slf4j 

    3. 中间转换包 ( 2.2.7 版本是没有使用的,  这里需要查一下相关资料)

       ![spring-boot-starter-logging](./img/03_004_springboot_log.png)

    4. 如果我们要引入其他框架, 需要把这个框架默认的日志框架移除

       例如: SpringFramework 使用 commons-logging ( 1.5 版本中, 2.x 版本好像没有发现)

       ![spring-boot-starter-logging](./img/03_005_springboot_log.png)

       SpringBoot 能自动适配所有日志,  而底层使用 slf4j  + logback , 引入其他框架的时候只需要排除其日志框架

## 3. logging 配置

> 官方文档-日志 : [4.4. Logging](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#boot-features-logging)

官方文档这样两句话值得注意

> Spring Boot uses Commons Logging for all internal logging ...
>
> By default, if you use the “Starters”, Logback is used ...

### 3.1 日志格式

![spring-boot-starter-logging](./img/03_006_springboot_log.png)

### 3.2 控制台输出

* 启用"debug"模式的两种方式:

  *  控制台输出	```$ java -jar myapp.jar --debug```
  *  application.properties 配置	```debug=true```

* 调试模式启动后日志跟踪模式
    * ```--trace```
	* 或者 ```trace=true```

* 彩色输出 (增加可读性)
	* 需要Terminal 支持 ANSI

* 自定义输出格式


* 配置涉及配置源码
```package org.springframework.boot.logging;````

### 3.3 文件输出
```

logging:
  #  日志文件输出 (默认 spring.log), 具体参考文档
  file:
#    path: C:\Temp\log\
#特定目录
    name: C:\Temp\log\my.log
#文件默认大小 10M, 默认大小自动被封
	max-Size: 10 
# 默认保存7天的日志	
	max-history: 7
```

### 3.4 日志级别


### 3.5 日志组

### 3.6  自定义日志配置
* 根据你的日志系统，下面的文件会被加载:

| Logging System | Customization |
| :--- | :---   |
| Logback  |    logback-spring.xml, logback-spring.groovy, logback.xml, <br>or logback.groovy<br> Logback-spring.xml、 logback-spring. groovy、 ogback.xml 或 logback.groovy      |
|  |   |


### 3.7 Logback 扩展

## 4. 切换日志框架
* 排除其他框架
* 参考 logblak 图解

# 四、Web 开发

## 1. 简介
* 使用 Springboot:

    1. 创建 SpringBoot 应用, 选中我们需要的模块;
    2. SpringBoot 默认将这些场景配置好了, 只需要在配置文件中少量配置;
    3. 编写业务代码.

* 自动配置原理 [重要]

    * 这个场景 SpringBoot 帮我们配置了什么? 能不能修改?  能修改哪些配置? 能不能扩展? ....

        ```
        XXXAutoConfiguration: 自动配置组件;
        xxxProperties : 配置类类封装配置文件的内容;
        ```

## 2.  SpringBoot 静态资源映射规则

```java
// 可以设置多喝静态资源的有关参数, 缓存时间
@ConfigurationProperties(
    prefix = "spring.resources",
    ignoreUnknownFields = false
)
public class ResourceProperties {
    private static final String[] CLASSPATH_RESOURCE_LOCATIONS = new String[]{ 
        "classpath:/META-INF/resources/",
         "classpath:/resources/", 
        "classpath:/static/",
         "classpath:/public/"
    };
}
```

```java
//org\springframework\boot\autoconfigure\web\servlet\WebMvcAutoConfiguration
// 添加资源映射(规则)
public void addResourceHandlers(ResourceHandlerRegistry registry) {
    if (!this.resourceProperties.isAddMappings()) {
        logger.debug("Default resource handling disabled");
    } else {
        Duration cachePeriod = this.resourceProperties.getCache().getPeriod();
        CacheControl cacheControl = this.resourceProperties.getCache().getCachecontrol().toHttpCacheControl();
        if (!registry.hasMappingForPattern("/webjars/**")) {
            this.customizeResourceHandlerRegistration(registry.addResourceHandler(new String[]{"/webjars/**"}).addResourceLocations(new String[]{"classpath:/META-INF/resources/webjars/"}).setCachePeriod(this.getSeconds(cachePeriod)).setCacheControl(cacheControl));
        }

        String staticPathPattern = this.mvcProperties.getStaticPathPattern();
        if (!registry.hasMappingForPattern(staticPathPattern)) {
            this.customizeResourceHandlerRegistration(registry.addResourceHandler(new String[]{staticPathPattern}).addResourceLocations(WebMvcAutoConfiguration.getResourceLocations(this.resourceProperties.getStaticLocations())).setCachePeriod(this.getSeconds(cachePeriod)).setCacheControl(cacheControl));
        }

    }
}

// 欢迎页映射配置
@Bean
public WelcomePageHandlerMapping welcomePageHandlerMapping(){
// 遍历找到 index.html
}

```

```java
// ResourceProperties.class

```

### 2.1  `/webjars／`　
    - 都去　｀classpath:/META-INF/resources/webjars/｀　找资源
    - webjars : 以 jar 包的方式引入静态资源,  https://www.webjars.org/
    - ![spring-boot-starter-logging](./img/04_001_webjars.png)
    - 直接访问静态文件: `http://localhost:81/webjars/jquery/3.5.1/jquery.js`
    
    ```xml
        <!-- 引入jquery-webjars-->
        <dependency>
            <groupId>org.webjars</groupId>
            <artifactId>jquery</artifactId>
            <version>3.5.1</version>
        </dependency>
    ```

### 2.2 "/**" 路径, 访问项目的任何资源, (静态资源的文件夹)
```
this.staticPathPattern = "/**";

"classpath:/META-INF/resources/",
"classpath:/resources/", 
"classpath:/static/",
"classpath:/public/"
"/" 当前项目的跟路径
```

### 2.3  欢迎页  webcome.index 都被/** 映射
````java
// WebMvcAutoConfiguration.class

````
### 2.4 所有的 **/favicon.ico 都在静态资源文件夹下查找


## 3. 模板引擎 Thymeleaf

* JSP , Veloclty, Freeemarker , Thymeleaf (SpringBoot 推荐)

![template engine](./img/04_002_template_engine.jpg)
![import template dependency ](./img/04_003_import_template_dependency.jpg)


[跳过]

## 4. Spring MVC 自动配置 [重要]

* 官方文档-[4.7. Developing Web Applications](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#boot-features-spring-mvc)
[2.2.9.RELEASE](https://docs.spring.io/spring-boot/docs/2.2.9.RELEASE/reference/html/spring-boot-features.html#boot-features-developing-web-applications)
* 官方文档-[Spring Web MVC](https://docs.spring.io/spring/docs/5.2.6.RELEASE/spring-framework-reference/web.html#mvc)

### 4.1 Spring MVC Auto-configuration

 [官方文档-自动配置SpringBoot 对 SpringMVC 的默认:  WebMvcAutoConfiguration ]

* Inclusion of `ContentNegotiatingViewResolver(视图解析器)` and `BeanNameViewResolver` beans.
    * 自动配置了 ViewResolver(视图解析器: 根据方法的返回值得到的势图对象, 即View, 视图对象决定如何渲染(转发or重定向) )
    * `ContentNegotiatingViewResolver`: 组合所有的视图解析器;
    * 如何实现定制ViewResolver: 实现 ViewResolver, 注入 bean , 然后会被自动组合进来;

    ```java
    // WebMvcAutoConfigurationAdapter.class
    @Bean
    @ConditionalOnBean(View.class)
    @ConditionalOnMissingBean
    public BeanNameViewResolver beanNameViewResolver() {}
    
    @Bean
    @ConditionalOnBean(ViewResolver.class)
    @ConditionalOnMissingBean(name = "viewResolver", value = ContentNegotiatingViewResolver.class)
    public ContentNegotiatingViewResolver viewResolver(BeanFactory beanFactory) {
        //  从 BeanFactory 中获取, 所以我们自定义的
    }
    
    // ContentNegotiatingViewResolver.class
    // 初始化方法
    @Override
    protected void initServletContext(ServletContext servletContext) {
            // 从容器( BeanFactory工具 )中获取所有的视图解析器
            // 我们定制的 ViewResolver 可以再这里被取出
            BeanFactoryUtils.beansOfTypeIncludingAncestors(obtainApplicationContext(), ViewResolver.class).values();
    }
    
    // 解析视图
    // 1. 获取候选视图对象
    // 2. 选择最合适的视图对象
    public View resolveViewName(String viewName, Locale locale) {}
    ```
  
* Support for serving static resources, including support for WebJars (covered later in this document)).
  
    * 静态文件资源 和 WebJars 支持
    
* Automatic registration of Converter, GenericConverter, and Formatter beans.(自动注册了 转换器, 格式化器)
    * Converter: 转换器 > public String hello(User user): 类转换器使用 Converter;
    * Formatter: 格式化器> "2017/09/12" 转换为 Date类型
    * **自定义转换器/格式化器: 同样放入容器中即可;**
    
    ```java
   // Automatic registration Formatter beans
    @Override
    public void addFormatters(FormatterRegistry registry) {
       // 格式化器添加到容器, 然后调用时候从 FactoryBean中获取格式化器
        ApplicationConversionService.addBeans(registry, this.beanFactory);
    }
   ```

*  Support for HttpMessageConverters (covered later in this document).
    * HttpMessageConverter:  SpringMVC 用来转换 Http 请求和响应的 :  一个方法返回 User对象, 我们想转 Json;
    * HttpMessageConverters : 从容器中确定(下面代码); 底层从容器中获取所有的 HttpMessageConverter
    * **自定义HttpMessageConverter , 只需要将自己的组件注册容器中(@Bean, @Component) (见文档HttpMessageConverters自定义实例)**
    
    ```java
   // 有参构造, 则从容器中获取
    public WebMvcAutoConfigurationAdapter(ResourceProperties resourceProperties, WebMvcProperties mvcProperties,
                    ListableBeanFactory beanFactory, ObjectProvider<HttpMessageConverters> messageConvertersProvider,
                    ObjectProvider<ResourceHandlerRegistrationCustomizer> resourceHandlerRegistrationCustomizerProvider) {
                this.resourceProperties = resourceProperties;
                this.mvcProperties = mvcProperties;
                this.beanFactory = beanFactory;
                this.messageConvertersProvider = messageConvertersProvider;
                this.resourceHandlerRegistrationCustomizer = resourceHandlerRegistrationCustomizerProvider.getIfAvailable();
            }
   ```
*  Automatic registration of `MessageCodesResolver ` (covered later in this document).
    * 定义错误码代码生成规则 
    * 比如 JSR 参数校验
    ```java
   // 定义了错误代码定义规则
    enum DefaultMessageCodesResolver.Format  getMessageCodesResolver() {}
   ```

*  Static index.html support.
  
*  Custom Favicon support (covered later in this document).
  
*  Automatic use of a `ConfigurableWebBindingInitializer` (可配置的Web绑定初始化程序) bean (covered later in this document).
    * 可以配置一个默认来替换 (添加到容器中)
    ```java
    初始化 web 数据绑定器 WebDataBinder (涉及类型转换, 格式化)
    getConfigurableWebBindingInitializer(){
       // 从容器中获取   
       return this.beanFactory.getBean(ConfigurableWebBindingInitializer.class);
   }
   // 初始化数据
   ConfigurableWebBindingInitializer.initBinder(WebDataBinder binder) {}
   ```

### 4.2 自动配置 jar 包 ( web 的自动配置场景)
* spring-boot-autoconfigure-2.2.7.RELEASE.jar
* org.springframework.boot.autoconfigure.web 中的`*.AutoConfiguration`类 

> If you want to keep those Spring Boot MVC customizations and make more MVC customizations (interceptors, formatters
> , view controllers, and other features), you can add your own @Configuration class of type WebMvcConfigurer but 
> without @EnableWebMvc. (保持 Spring Boot MVC 的定制, 并且添加定制;  全面接管 Spring MVC,   @EnableWebMvc 用于全面接管 SpringMVC)

> If you want to provide custom instances of RequestMappingHandlerMapping, RequestMappingHandlerAdapter, 
> or ExceptionHandlerExceptionResolver, and still keep the Spring Boot MVC customizations, you can declare a bean of
> type WebMvcRegistrations and use it to provide custom instances of those components.  

>> If you want to take complete control of Spring MVC, you can add your own @Configuration annotated with @EnableWebMvc,
> or alternatively add your own @Configuration-annotated DelegatingWebMvcConfiguration as described in the Javadoc of 
> @EnableWebMvc.

### 4.3 扩展 SpringMVC

* 传统 SpringMVC 的XML配置 
```xml
<mvc:view-controller path="/hello" view-name="success"/>
<mvc:interceptors>
    <mvc:interceptor>
        <mvc:mapping path="/hello"/>
        <bean></bean>
    </mvc:interceptor>
</mvc:interceptors>
```
* `implements WebMvcConfigurer`

* `@Deprecated WebMvcConfigurerAdapter` 是在 springboot v1.0 版本中;
 现在 springboot 2.x 和 Spring 5.0 已经废弃使用, 推荐继承 `WebMvcConfigurationSupport`(疑似会导致自动配置失效)
 或 实现 `WebMvcConfigurer`

* 代码实现 [./config/MyMvcConfig.java](./spring-boot-04-web-restfulcrud/src/main/java/com/xm/springboot/config/MyMvcConfig.java)
    * 编写一个实现 `WebMvcConfigurer`接口的类, 并注解 `@Configuration` (不能标注 `@EnableWebMvc`)
    * 既实现自定义扩展 ,也保留了 SpringBoot 的自动配置
* 实现原理 [Google 云盘 UML 图]
    * 1. `WebMvcAutoConfiguration` 是SpringMVC  自动配置类
    * 2. 在坐其他配置的时候会 `@Import(EnableWebMvcConfiguration.class)` ,  见 UML 图 [扩展Spring MVC]
    * 3. 容器中的所有 WebMvcConfigurer 都会起作用, 包括我们自己定义的也会被调用

### 4.4 全面接管 SpringMVC     

    自定义配置类启用 `@EnableWebMvc` 来使的 `WebMvcAutoConfiguration` 失效, 从而实现全面接管 SpringMVC, 都是我们一般开发中不推荐这样使用

* 代码实现:
  
* 在扩展的基础之上增加 `@EnableWebMvc` 注解
  
* 实现原理 [Google 云盘 UML 图]
    * `@Interface EnableWebMvc` 引入了这个类
    ```java
    @Import({DelegatingWebMvcConfiguration.class})
    public @interface EnableWebMvc {
    }
    ```
    * 这个类导致自动配置类失效
    `@ConditionalOnMissingBean({WebMvcConfigurationSupport.class})`
    ```java
    // 当容器不存在这个 Bean 组件时才生效 
    @ConditionalOnMissingBean({WebMvcConfigurationSupport.class})
    public class WebMvcAutoConfiguration {}  
    ```
    * 这只是其中一点功能

## 5. 如何修改`SpringBoot`的默认配置

模式: 
* 1. SpringBoot 在自动配置很多组件的时候先看有没有用户配置的(`@Bean`, `@Component`) 如果有就用用户配置的, 
如果没有就用自动配置的;  如果组件有多个 `ViewResolver`将用户配置的和自己默认的组合起来使用;

* 2. 在 SpringBoot中有非常多的`xxConfigurer`帮助我们自动配置.
* 3. 在 SpringBoot中有非常多的`xxCustomizer`帮助我们定制配置.


## 6. Restful CRUD

### 6.1. 默认访问首页
```java
//  在 自定义配置类中加入方法
@Bean
public WebMvcConfigurer webMvcConfigurer() {
    WebMvcConfigurer  configurer = new WebMvcConfigurer(){
        @Override
        public void addViewControllers(ViewControllerRegistry registry) {
            registry.addViewController("/").setViewName("login");
            registry.addViewController("/index.html").setViewName("login");
        }
    };

    return configurer;

}
```

### 6.2. 国际化

#### 1. 实现
**1. 编写国际化配置文件** 
2. 使用 ResourceBundleMessageSource 管理国际化资源文件
3. 在页面(jsp)使用 fmt:message 取出国际化内容
4. 效果: 根据浏览器的语言设置来切换国际化
    * chrome 设置>高级>语言 `chrome://settings/languages` 将 `英语 (美国)` 移到顶部

#### 2. 点击链接实现国际化

* 实现一个 LocalResolver

// todo

### 6.3. 登录

#### 1. 模板引擎缓存
开发期间 模板引擎页面修改后, 避免缓存给, 要即时生效
    * 禁用模板引擎的缓存g
    * Ctrl + F9 , 重新编译;

#### 2. 拦截器进行登录检查

* 实现 `HandlerInterceptor` 接口, 自定义登录拦截器
* 注册拦截器到自定义 `WebMvcConfigurer` 中
    * 需要手动排除静态资源请求
        ```java
        // v2.0 需要手动排除静态资源映射
        .excludePathPatterns("/asserts/**")
        .excludePathPatterns("/webjars/**");
        ```

### 6.4 Restful CRUD

#### 1. Restful 规范

#### 2. Restful 请求架构 

| 实验功能 | 请求URI | 请求方式 |
| --- | --- |
| 查询所有员工 | emps | GET |
| 查询某个员工(修改页面) | emp/1 | GET |
| 来到添加页面 |  emp | GET |
| 添加员工 | emp | POST |
| 来到修改页面(查出员工显示) | emp/1 | GET |
| 修改员工 | emp | PUT |
| 删除员工  | emp/1 | DELETE |


#### 3. 员工列表

##### thymeleaf 公共元素的抽取

* [8 Template Layout](https://www.thymeleaf.org/doc/tutorials/3.0/usingthymeleaf.html#template-layout)

1. 抽取公共片段

```html
<div th:fragment="copy">
  &copy; 2011 The Good Thymes Virtual Grocery
</div>
```

2. 引入公共片段

```html
<div th:insert="~{footer :: copy}"></div>

the (~{,}) enclosing is completely optional, so the code above would be equivalent to:

<div th:insert="footer :: copy"></div>
```

* Fragment specification syntax
    * `"~{templatename::selector}"` or `"~{templatename::fragmentname}"`
    * `"~{templatename}"`
    * `"~{::selector}"` or `"~{this::selector}"` 

* Difference between th:insert and th:replace (and th:include, not recommended since 3.0)
    * `th:insert` is the simplest: it will simply insert the specified fragment as the body of its host tag.
    * `th:replace` actually replaces its host tag with the specified fragment.
    * `th:include` is similar to th:insert, but instead of inserting the fragment it only inserts the contents of this fragment.

##### forward 和 redirect

```

```

##### SpringMVC 中的 `date-format` (增加员工)
* SpringMVC 中将页面提交的值转换为指定类型 
* 类型转换, 格式化

* 例如: `2020/02/02` 转换为 `Date`,  我们需要需改可以支持的格式为 `2020-02-02`

**查看源码: **
(springboot 1.x  )
```
// 1.  org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration.EnableWebMvcConfiguration
@Bean
@Override
public FormattingConversionService mvcConversionService() {
    WebConversionService conversionService = new WebConversionService(this.mvcProperties.getDateFormat());
    addFormatters(conversionService);
    return conversionService;
}

// 2. org.springframework.boot.autoconfigure.web.servlet.WebMvcProperties

@ConfigurationProperties(prefix = "spring.mvc")
public class WebMvcProperties {
	/**
	 * Date format to use. For instance, `dd/MM/yyyy`.
	 */
	private String dateFormat;
}

// 3. 在配置文件中自定义格式
spring.mvc.date-format=yyyy-MM-dd
```

##### 修改员工 .  `PUT` 方法

发送 put 请求修改员工数据

1. SpringMVC 中配置 `hiddenHttpMethodFilter` ; (Spring Boot 已经自动配置)
2. 页面创建一个 post 表单
3. 添加一个 `name="_method"` 的 `input` 项, 值就是我们指定的请求方式

## 7. 错误处理机制

### 7.1 Spring boot 默认错误处理机制

**默认效果**

* 对于浏览器网页(html) 响应 HTML 页面
* 对于客户端(json) 响应 json 数据
* 主要判断依据为 `Accept: text/html` 和 `Accept:**/**` 来判断是浏览器还是客户端

```
GET http://localhost:81/crud/ddd
#Accept: text/html  # 浏览器, 返回 html 页面
Accept: */*         # 客户端, 返回 json 数据
```

**ErrorMvcAutoConfiguration** 错误处理的自动配置类

`org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration` 

**自动配置类向容器中添加的几个处理组件:**

* `DefaultErrorAttributes` 默认获取的错误消息
```java

@Order(-2147483648)
public class DefaultErrorAttributes implements ErrorAttributes, HandlerExceptionResolver, Ordered {
    public Map<String, Object> getErrorAttributes(WebRequest webRequest, boolean includeStackTrace) {
        Map<String, Object> errorAttributes = new LinkedHashMap();
        errorAttributes.put("timestamp", new Date());
        this.addStatus(errorAttributes, webRequest);
        this.addErrorDetails(errorAttributes, webRequest, includeStackTrace);
        this.addPath(errorAttributes, webRequest);
        return errorAttributes;
    }
}
```

    **页面可以获取的默认信息:**
    
    * timestamp : 时间戳
    * status: 状态码
    * error: 错误提示
    * exception : 异常对象 (ErrorDetails)
    * message: 异常消息
    * errors: JSR303 数据校验的错误


* `BasicErrorController`  处理 `/error` 请求

```java
@Controller
// 如果没有配置 server.error.path, 则获取 error.path, 也没有则 使用 /error
// 例如: server.error.path=/error
@RequestMapping("${server.error.path:${error.path:/error}}")
public class BasicErrorController extends AbstractErrorController {
    // 处理浏览器发送的请求, 并返回 html 页面
    // TEXT_HTML_VALUE = "text/html";
	@RequestMapping(produces = MediaType.TEXT_HTML_VALUE)
	public ModelAndView errorHtml(HttpServletRequest request, HttpServletResponse response) {
		HttpStatus status = getStatus(request);
		Map<String, Object> model = Collections
				.unmodifiableMap(getErrorAttributes(request, isIncludeStackTrace(request, MediaType.TEXT_HTML)));
		response.setStatus(status.value());

        // 要显示的错误页面, 包含请求地址其中的内容
		ModelAndView modelAndView = resolveErrorView(request, response, status, model);
        // 没有模板引擎, 则使用 Spring boot 默认的视图页面 ModelAndView("error")
		return (modelAndView != null) ? modelAndView : new ModelAndView("error", model);
	}


	// 对客户端请求,返回 json 数据
	@RequestMapping
	public ResponseEntity<Map<String, Object>> error(HttpServletRequest request) {
		HttpStatus status = getStatus(request);
		if (status == HttpStatus.NO_CONTENT) {
			return new ResponseEntity<>(status);
		}
		Map<String, Object> body = getErrorAttributes(request, isIncludeStackTrace(request, MediaType.ALL));
		return new ResponseEntity<>(body, status);
	}


    // SpringBoot 默认错误视图
	@Configuration(proxyBeanMethods = false)
	@ConditionalOnProperty(prefix = "server.error.whitelabel", name = "enabled", matchIfMissing = true)
	@Conditional(ErrorTemplateMissingCondition.class)
	protected static class WhitelabelErrorViewConfiguration {

	    // 默认错误视图
		private final StaticView defaultErrorView = new StaticView();

		@Bean(name = "error")
		@ConditionalOnMissingBean(name = "error")
		public View defaultErrorView() {
			return this.defaultErrorView;
		}

		// If the user adds @EnableWebMvc then the bean name view resolver from
		// WebMvcAutoConfiguration disappears, so add it back in to avoid disappointment.
		@Bean
		@ConditionalOnMissingBean
		public BeanNameViewResolver beanNameViewResolver() {
			BeanNameViewResolver resolver = new BeanNameViewResolver();
			resolver.setOrder(Ordered.LOWEST_PRECEDENCE - 10);
			return resolver;
		}

	}

}
```


* `ErrorPageCustomizer` 注册服务器的错误页面

```java
/**
 * {@link WebServerFactoryCustomizer} that configures the server's error pages.
 */
private static class ErrorPageCustomizer implements ErrorPageRegistrar, Ordered {
    @Override
    public void registerErrorPages(ErrorPageRegistry errorPageRegistry) {
        // 注册错误页面
        ErrorPage errorPage = new ErrorPage(
                this.dispatcherServletPath.getRelativePath(this.properties.getError().getPath()));
        errorPageRegistry.addErrorPages(errorPage);
    }

}
// this.properties.getError().getPath()
public class ErrorProperties {
    // 系统出现错误以后来到error请求进行处理；（web.xml注册的错误页面规则）

	/**
	 * Path of the error controller.
	 */
	@Value("${error.path:/error}")
	private String path = "/error";
}
```

* `DefaultErrorViewResolver` 默认错误视图解析器

默认ErrorViewResolver ，试图以纠正错误的观点使用众所周知的约定执行。 
将搜索下模板和静态资产`/error` 使用 `status code` 和 `status series` 。
例如， HTTP 404将搜索（在特定的顺序）：

```
'/<templates>/error/404.<ext>'
'/<static>/error/404.html'
'/<templates>/error/4xx.<ext>'
'/<static>/error/4xx.html'
```

```java
public class DefaultErrorViewResolver implements ErrorViewResolver, Ordered {
    // 解析视图
	@Override
	public ModelAndView resolveErrorView(HttpServletRequest request, HttpStatus status, Map<String, Object> model) {
	    // 直接根据 http 状态码解析 ModelAndView
        ModelAndView modelAndView = resolve(String.valueOf(status.value()), model);
		if (modelAndView == null && SERIES_VIEWS.containsKey(status.series())) {
            // 如果没有解析到对应的错误页面, 则直接使用默认的 4xx, 5xx页面
			modelAndView = resolve(SERIES_VIEWS.get(status.series()), model);
		}
		return modelAndView;
	}

	private ModelAndView resolve(String viewName, Map<String, Object> model) {
        // 默认Springboot 去找`error/`下的一个页面  eg: error/404
		String errorViewName = "error/" + viewName;

        // 先使用模板引擎解析
		TemplateAvailabilityProvider provider = this.templateAvailabilityProviders.getProvider(errorViewName,
				this.applicationContext);
		if (provider != null) {
            // 判断: 模板引擎可用, 返回到 `errorViewName` 指定额默认视图地址
			return new ModelAndView(errorViewName, model);
		}
		// 模板引擎不可用, 就从静态资源文件夹下寻找 errorViewName 对应的页面  eg: error/404.html
		return resolveResource(errorViewName, model);
	}

	private ModelAndView resolveResource(String viewName, Map<String, Object> model) {
		for (String location : this.resourceProperties.getStaticLocations()) {
			try {
				Resource resource = this.applicationContext.getResource(location);
				resource = resource.createRelative(viewName + ".html");
				if (resource.exists()) {
					return new ModelAndView(new HtmlResourceView(resource), model);
				}
			}
			catch (Exception ex) {
			}
		}
		return null;
	}
}
```


**处理步骤**

1. `ErrorPageCustomizer` 定义错误响应规则), 系统抛出 4xx, 5xx 错误, 发起`/error` 请求
2. `BasicErrorController`  处理 `/error` 请求
3. 具体去那个页面, 由 `DefaultErrorViewResolver` 解析处理

### 7.2 如何定制错误响应

#### 1) 定制错误页面

* 使用模板引擎的情况: `error/状态码`

将错误页面命名为 错误状态码.html 放在模板引擎文件夹里面的 error 文件夹下】，发生此状态码的错误就会来到 对应的页面；

我们可以使用4xx和5xx作为错误页面的文件名来匹配这种类型的所有错误，精确优先（优先寻找精确的状态
码.html）;

* 没有模板引擎, 使用静态资源文件夹 (只是这是就不能直接获取默认错误信息了)

* 都没有, 则默认 SpringBoot 默认的错误提示页面

    **页面可以获取的默认信息:**
    
    * timestamp : 时间戳
    * status: 状态码
    * error: 错误提示
    * exception : 异常对象 (ErrorDetails)
    * message: 异常消息
    * errors: JSR303 数据校验的错误

#### 2) 定制错误 JSON 数据

1. 自定义异常处理 & 返回定制 JSON 数据

* 利用 `@ExceptionHandler` 注解 
* 缺点: 浏览器和客户端都返回的 JSON 数据 (这里 springboot v2.x 中貌似已经有自适应处理)

```java
// @ControllerAdvice

@ResponseBody
@ExceptionHandler(UserNotExistException.class)
public Map<String, Object> handelrException(Exception e) {
    Map<String, Object> map = new HashMap<>();
    map.put("code", user.notexist);
    map.put("message", e.getMessage());
    return map;
}
```

* v2.x 中的处理情况

```http request
GET http://localhost:81/crud/hello?user=null
#Accept: text/html  // 这里自动转发到 `/error` 处理了
Accept: */* // 直接返回自定义异常处理类定制的 JSON 数据
```

2. 转发到 `/error` 进行自适应响应处理

* (这里只是向 `/error` 中传递自己定义的数据 )
* 添加 request 属性, 转发到自定义的错误页面

```java
@ExceptionHandler(UserNotExistException.class)
public String hanlrException(Exception e, HttpServletRequest request) {
    Map<String, Object> map = new HashMap<>();
    // 传入我们自己的错误码, 进入自定义页面
    /**
     * 		Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
     */
    request.setAttribute("javax.servlet.error.status_code", 520);

    map.put("code", 502);
    map.put("message", e.getMessage());
    return "forward:/error";
}
```

```http request
### 2. 转发到 `/error` 进行自适应响应处理
GET http://localhost:81/crud/hello?user=null
Accept: text/html  // 这里自动转发到 `/error` 处理了
#Accept: */* // 转发到 `/error` 后自适应处理的 JSON 数据
```

3. 将我们的定制的数据携带出去

* 出现错误后会进入 '/error' 请求, 会被 `` 处理, 而响应出去可以获取的数据是由 `getErrorAttributes()` 得到的 (是 `AbstractErrorController 
implements ErrorController` 规定的方法);

* 方法一: 完全编写一个 `ErrorController 的实现类 ( 或者 AbstractErrorController 的子类 )` 在容器中 (太麻烦);
* 方法二: 页面上能用的数据, 或者 JSON 返回能用的数据 都是通过 `this.errorAttributes.getErrorAttributes()` 得到:
    容器中的 `DefaultErrorAttributes.getErrorAttributes()` 默认 进行数据处理;
    自定义 `ErrorAttributes`: `com.xm.springboot.component.MyErrorAttributes`

## 8. 嵌入式 Servlet 容器配置修改

* Spring Boot 默认使用 Tomcat 作为嵌入式 Servlet 容器;

### 8.1 如何定制和修改 Servlet 容器的相关配置;

1. 修改刚 `servlet` 有关的配置 (`ServerProperties.java`) 
```properties
server.port=80
server.context-patch=/crud

// 通用的 Servlet 容器配置
server.xxx
// Tomcat 配置
server.tomcat.xxx
```

2. 自定义 `WebServerFactoryCustomizer<ConfigurableWebServerFactory>`, 嵌入式 Sevlet 容器的定制器来修改 Servlet 容器的配置;

* Spring Boot v2.0 以上版本 `EmbeddedServletContainerCustomizer`被`WebServerFactoryCustomizer`替代

* `config.MyMvcConfig.webServerFactoryCustomizer()`
```java
@Bean
public WebServerFactoryCustomizer<ConfigurableWebServerFactory> webServerFactoryCustomizer() {
        return new WebServerFactoryCustomizer<ConfigurableWebServerFactory>() {
            @Override
            public void customize(ConfigurableWebServerFactory factory) {
                // 覆盖了 配置文件中的 servlet.port
                factory.setPort(8092);
            }
        };
}

```

### 8.2 注册三大组件: 

* 1. ServletRegistrationBean

```java
@Bean
public ServletRegistrationBean mySevlet() {
    ServletRegistrationBean<MyServlet> registration = new ServletRegistrationBean(new MyServlet(), "/myServlet");
    registration.setLoadOnStartup(1);

    return registration;
}
```

* 2. FilterRegistrationBean

```java
@Bean
public FilterRegistrationBean myFilter() {
    FilterRegistrationBean filter = new FilterRegistrationBean();
    filter.setFilter(new MyFilter());
    filter.setUrlPatterns(Arrays.asList("/hello", "/myServlet"));
    return filter;
}
```

* 3. ServletListenerRegistrationBean

```java
@Bean
public ServletListenerRegistrationBean servletListener() {
    ServletListenerRegistrationBean<MyListener> listener = new ServletListenerRegistrationBean(new MyListener());
    return listener;
}
```

### 8.3 嵌入其他嵌入式 Servlet 容器

![](img/Snipaste_2020-10-30_07-49-42.jpg)

* Tomcat 默认

```xml
<!--引入 web   默认使用 tomcat -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>
```

* Jetty (适合长连接, 如 点对点聊天)

```xml
<!--引入 web-->
<dependency>
<groupId>org.springframework.boot</groupId>
<artifactId>spring-boot-starter-web</artifactId>
<exclusions>
    <exclusion>
        <artifactId>spring-boot-starter-tomcat</artifactId>
        <groupId>org.springframework.boot</groupId>
    </exclusion>
</exclusions>
</dependency>
<!--引入其他 Servlet 容器-->
<dependency>
<groupId>org.springframework.boot</groupId>
<artifactId>spring-boot-starter-jetty</artifactId>
</dependency>
```

* * Undertow (并发性能好)

```xml
<!--引入 web-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
    <exclusions>
        <exclusion>
            <artifactId>spring-boot-starter-tomcat</artifactId>
            <groupId>org.springframework.boot</groupId>
        </exclusion>
    </exclusions>
</dependency>
<!--引入其他 Servlet 容器-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-jetty</artifactId>
</dependency>
```


### 8.4 嵌入式 Servlet 容器自动配置原理

```EmbeddedWebServerFactoryCustomizerAutoConfiguration.java```
(package org.springframework.boot.autoconfigure.web.embedded)



```java
@Configuration(proxyBeanMethods = false)
@AutoConfigureOrder(Ordered.HIGHEST_PRECEDENCE)
// 当 ServletRequest 类存在于 classpath 时有效	
@ConditionalOnClass(ServletRequest.class)
// 仅在当前应用是 Servlet Web 应用时才生效
@ConditionalOnWebApplication(type = Type.SERVLET)
@EnableConfigurationProperties(ServerProperties.class)
// 导入  TomcatServletWebServerFactory, JettyServletWebServerFactory, UndertowServletWebServerFactory
@Import({ ServletWebServerFactoryAutoConfiguration.BeanPostProcessorsRegistrar.class,
		ServletWebServerFactoryConfiguration.EmbeddedTomcat.class,
		ServletWebServerFactoryConfiguration.EmbeddedJetty.class,
		ServletWebServerFactoryConfiguration.EmbeddedUndertow.class })
public class ServletWebServerFactoryAutoConfiguration {

}
```





### 8.5 嵌入式 Servlet 容器启动原理





# 五、Docker

# 六、Spring boot 与数据访问

## 1. JDBC

```xml
<dependencies>
    <!-- jdbc -->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-jdbc</artifactId>
    </dependency>
    <!-- mysql -->
    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
        <scope>runtime</scope>
    </dependency>
</dependencies>		
```

```yaml
spring:
  datasource:
    username: root
    password: 123456
    url: jdbc:mysql://ip:port/schema
    driver-class-name: com.mysql.cj.jdbc.Driver
```

* 默认使用数据源(v2) : `class com.zaxxer.hikari.HikariDataSource`
* 数据源配置类 : `org.springframework.boot.autoconfigure.jdbc.DataSourceProperties`

**自动配置原理**

`package org.springframework.boot.autoconfigure.jdbc;`

1. `DataSourceConfiguration` 根据配置创建数据源,  使用`spring.datasource.type` 指定数据源;

2. 默认支持 `Tomcat Pool DataSource`, `HikariDataSource`, `Dbcp2 DataSource`;

3. 自定义数据源:

   ```java
   
   // DataSourceConfiguration.dataSource()
   /**
   * Generic DataSource configuration.
   */
   @Configuration(proxyBeanMethods = false)
   @ConditionalOnMissingBean(DataSource.class)
   @ConditionalOnProperty(name = "spring.datasource.type")
   static class Generic {
   
   @Bean
   DataSource dataSource(DataSourceProperties properties) {
       // 使用 DataSourceBuilder 创建数据源, 使用反射响应 type 的数据源, 并绑定相关属性
   	return properties.initializeDataSourceBuilder().build();
   }
   
       
   // DataSourceBuilder.builde()
   public T build() {
       Class<? extends DataSource> type = this.getType();
       DataSource result = (DataSource)BeanUtils.instantiateClass(type);
       this.maybeGetDriverClassName();
       this.bind(result);
       return result;
   }
   ```

4. 数据源初始化

   **初始化原理 :**

   ```java
   // 1. 导入 DataSourceInitializerInvoker 组件
   @Import({ DataSourceInitializerInvoker.class, DataSourceInitializationConfiguration.Registrar.class })
   class DataSourceInitializationConfiguration {
   }
   
   /**
   2. 初始化后运行 schema-.sql 和 data-.sql 文件
   Bean to handle {@link DataSource} initialization by running {@literal schema-.sql} on {@link InitializingBean#afterPropertiesSet()} and {@literal data-.sql} SQL scripts on a {@link DataSourceSchemaCreatedEvent}.
    **/
   class DataSourceInitializerInvoker implements ApplicationListener<DataSourceSchemaCreatedEvent>, InitializingBean {
       
       
       
       // 3. 实现 InitializingBean 的 afterPropertiesSet(), 
       // 实际就是执行 DDL 脚本, 配置 spring.datasource.schema 或 schema 指定
       @Override
   	public void afterPropertiesSet() {
   		DataSourceInitializer initializer = getDataSourceInitializer();
   		if (initializer != null) {
               
               // 3.1. 执行 DataSourceInitializer.createSchema()
   			boolean schemaCreated = this.dataSourceInitializer.createSchema();
   			if (schemaCreated) {
   				initialize(initializer);
   			}
   		}
   	}
       
       // 4. 实现 ApplicationListener (监听器) 的 onApplicationEvent() 
       // 实际就是执行 DML 脚本, 配置 spring.datasource.data 或 data 指定SQL文件
       @Override
   	public void onApplicationEvent(DataSourceSchemaCreatedEvent event) {
   		// NOTE the event can happen more than once and
   		// the event datasource is not used here
   		DataSourceInitializer initializer = getDataSourceInitializer();
   		if (!this.initialized && initializer != null) {
               // 4.1 执行 DataSourceInitialize.initSchema()
   			initializer.initSchema();
   			this.initialized = true;
   		}
   	}
   }
   
   class DataSourceInitializer {
       // Create the schema if necessary.
   	boolean createSchema() {
           
           // 3.2. createSchema() 方法中 getScripts() 
           // 获取 sql 文件 （platform="all"） classpath*:schema-all.sql 和 classpath*:schema.sql (fallback resources 后备资源? 预备资源? 大概是这个意思)
           
   		List<Resource> scripts = getScripts("spring.datasource.schema", this.properties.getSchema(), "schema");
   		if (!scripts.isEmpty()) {
   			if (!isEnabled()) {
   				logger.debug("Initialization disabled (not running DDL scripts)");
   				return false;
   			}
   			String username = this.properties.getSchemaUsername();
   			String password = this.properties.getSchemaPassword();
               
              	// 3.3. 运行获取的 sql 文件
   			runScripts(scripts, username, password);
   		}
   		return !scripts.isEmpty();
   	}
       
       
      /**
   	 * Initialize the schema if necessary.
   	 * @see DataSourceProperties#getData()
   	 */
   	void initSchema() {
           // 4.2. 获取配置
   		List<Resource> scripts = getScripts("spring.datasource.data", this.properties.getData(), "data");
   		if (!scripts.isEmpty()) {
   			if (!isEnabled()) {
   				logger.debug("Initialization disabled (not running data scripts)");
   				return;
   			}
   			String username = this.properties.getDataUsername();
   			String password = this.properties.getDataPassword();
   			runScripts(scripts, username, password);
   		}
   	}
   }
   ```

   **初始化配置: **

   在 classpath 下创建 sql 文件 schema-*.sql 和 data-*.sql

   默认规则 schema.sql, schema-all.sql
   

或指定自定义文件名称可以使用:

   ```yaml
   schema:
   	-classpath:department.sql   
   ```

注意: 我这里在classpath 下创建了含有 DDL 语句的 `schema-all.sql`文件, 然后运行却没有创建表, debug 后可以发现实际时没有启用  ( 注意看上面代码 3.2 / 4.2 中的 isEnabled() ) ,  所以要使用 `initialization-mode: always` 来启用

```java
   	private boolean isEnabled() {
   		DataSourceInitializationMode mode = this.properties.getInitializationMode();
   		if (mode == DataSourceInitializationMode.NEVER) {
   			return false;
   		}
   		if (mode == DataSourceInitializationMode.EMBEDDED && !isEmbedded()) {
   			return false;
   		}
   		return true;
   	}
   	
   spring:
     datasource:
       initialization-mode: always
```

5. 查询示例

```java

```


## 2. 整合Druid 数据源

1. 导入 maven 依赖: [Druid Spring Boot Starter](https://mvnrepository.com/artifact/com.alibaba/druid-spring-boot-starter)

   ```
   <dependency>
       <groupId>com.alibaba</groupId>
       <artifactId>druid-spring-boot-starter</artifactId>
       <version>${druid-version}</version>
   </dependency>
   ```

2. 修改 `spring.datasource.type=com.alibaba.druid.pool.DruidDataSource`

3. 其他配置见 druid-spring-boot-starter 配置说明

   ```
   spring:
     datasource:
       username: root
       password: 123456
       url: jdbc:mysql://ip:port/jdbc
       driver-class-name: com.mysql.cj.jdbc.Driver
       hikari:
         # Possibly consider using a shorter maxLifetime value. 问题添加
         max-lifetime: 1000000
       type: com.alibaba.druid.pool.DruidDataSource
   
       # Druid 配置属性  https://github.com/alibaba/druid/tree/master/druid-spring-boot-starter
       druid:
         initial-size: 5
       # 监控配置
         # 配置StatFilter
         filter:
           stat:
             enabled: true
             db-type: h2
             # 慢SQL统计
             log-slow-sql: true
             # 慢SQL时间规则 ms
             slow-sql-millis: 10
         # WebStatFilter配置，说明请参考Druid Wiki，配置_配置WebStatFilter
         web-stat-filter:
           #是否启用StatFilter默认值false
           enabled: true
           # 添加过滤规则
           url-pattern: /*
           # 排除一些不必要的url
           exclusions: "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*"
           # 开启session统计功能
           session-stat-enable: true
           #session-stat-max-count:
           #principal-session-name:
           #principal-cookie-name:
           #profile-enable:
         # StatViewServlet置，说明请参考Druid Wiki，配置_StatViewServlet配置
         stat-view-servlet:
           #是否启用StatViewServlet（监控页面）默认值为false（考虑到安全问题默认并未启动，如需启用建议设置密码或白名单以保障安全）
           enabled: true
           # 内置监控页面的地址
           url-pattern: /druid/*
           #关闭 Reset All 功能
           reset-enable: false
           # 设置用户名
           login-username: admin
           # 设置密码
           login-password: ad12min
           # 白名单
           allow: 127.0.0.1
           # 黑名单
           #deny:
         # 配置多个英文逗号分隔, 内置Filter的别名 https://github.com/alibaba/druid/wiki/%E5%86%85%E7%BD%AEFilter%E7%9A%84%E5%88%AB%E5%90%8D
         filters: stat
   ```

   

* [Druid Wiki  常见问题](https://github.com/alibaba/druid/wiki/%E5%B8%B8%E8%A7%81%E9%97%AE%E9%A2%98)
* [使用 Druid Spring Boot Starter](https://github.com/alibaba/druid/tree/master/druid-spring-boot-starter)
* [SpringBoot - 开启Druid监控统计功能教程（SQL监控、慢SQL记录、去广告）](https://www.hangge.com/blog/cache/detail_2876.html#)

 

## 3. 整合Mybatis






## 4. Spring JPA



# 七、SpringBoot 启动配置原理



## 启动流程

```java
1.创建对象
2.执行 run()
return new SpringApplication(primarySources).run(args);
```

1. 创建 `SpringApplication` 对象

   ```java
   this.resourceLoader = resourceLoader;
   Assert.notNull(primarySources, "PrimarySources must not be null");
   // 保存 primarySource
   this.primarySources = new LinkedHashSet<>(Arrays.asList(primarySources));
   // 推断项目类型, (这里是 WebApplicationType.SERVLET, servlet web项目 )
   this.webApplicationType = WebApplicationType.deduceFromClasspath();
   // 从Spring工厂示例中读取
   this.bootstrappers = new ArrayList<>(getSpringFactoriesInstances(Bootstrapper.class));
   // 从配置文件 META-INF/spring.factories 中获取 Application Context Initializers
   setInitializers((Collection) getSpringFactoriesInstances(ApplicationContextInitializer.class));
   // 从配置文件 META-INF/spring.factories 中获取 Application Listeners
   setListeners((Collection) getSpringFactoriesInstances(ApplicationListener.class));
   // 推断 main 方法
   this.mainApplicationClass = deduceMainApplicationClass();
   ```
```
   
   ```java
   private <T> Collection<T> getSpringFactoriesInstances(Class<T> type, Class<?>[] parameterTypes, Object... args) {
       ClassLoader classLoader = getClassLoader();
       // Use names and ensure unique to protect against duplicates
       Set<String> names = new LinkedHashSet<>(SpringFactoriesLoader.loadFactoryNames(type, classLoader));
       List<T> instances = createSpringFactoriesInstances(type, parameterTypes, classLoader, args, names);
       AnnotationAwareOrderComparator.sort(instances);
       return instances;
   }
```

**META-INF/spring.factories 配置文件**

* Application Context Initializers

  

* Application Listeners

![](./img/Snipaste_2021-01-17_11-40-25.jpg)



2. 运行 `run`方法, 启动应用





## 运行流程



## 自动配置原理





**TODO**

- [ ] 利用软件设计之美 模型 接口 [实现] 来理解

---

https://api.bilibili.com/x/player/pagelist?bvid=BV1Ct411g78e

## 说明

- `官方文档-xx模块` --- 方便从搜索查询

**TAG**

- [重要]
```

```