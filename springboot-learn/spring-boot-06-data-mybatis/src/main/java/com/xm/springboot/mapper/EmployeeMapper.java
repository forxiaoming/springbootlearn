package com.xm.springboot.mapper;

import com.xm.springboot.bean.Employee;
import org.apache.ibatis.annotations.Mapper;

/**
 * 使用配置文件
 *
 * @author xiaoming
 * @date 2020-12-01 22:03
 */
 // @Mapper  @MapperScan 将接口配置文件装载到容器中

@Mapper
public interface EmployeeMapper {

    public  Employee getEmpById(Integer id);

    public  void insertEmp(Employee employee);

}
