package com.xm.springboot.config;

import org.apache.ibatis.session.Configuration;
import org.mybatis.spring.boot.autoconfigure.ConfigurationCustomizer;
import org.springframework.context.annotation.Bean;

/**
 * MyBatis 自定义配置
 *
 * @author xiaoming
 * @date 2020-11-29 15:59
 */
@org.springframework.context.annotation.Configuration
public class MybatisConfig {

    @Bean
    public ConfigurationCustomizer configurationCustomizer() {
        return  new ConfigurationCustomizer() {
            @Override
            public void customize(Configuration configuration) {
                // 开启驼峰命名, 如: department_name -- 映射 ---> departmentName
                configuration.setMapUnderscoreToCamelCase(true);
            }
        };

    }
}
