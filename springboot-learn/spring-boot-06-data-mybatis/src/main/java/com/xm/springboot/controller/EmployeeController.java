package com.xm.springboot.controller;

import com.xm.springboot.bean.Employee;
import com.xm.springboot.mapper.EmployeeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xiaoming
 * @date 2020-12-03 21:31
 */
@RestController
public class EmployeeController {

    @Autowired
    EmployeeMapper employeeMapper;

    @GetMapping("/emp/{id}")
    public Employee getEmp(@PathVariable Integer id) {
        return employeeMapper.getEmpById(id);
    }
}
