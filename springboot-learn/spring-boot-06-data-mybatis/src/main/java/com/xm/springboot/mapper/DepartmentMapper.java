package com.xm.springboot.mapper;

import com.xm.springboot.bean.Department;
import org.apache.ibatis.annotations.*;

/**
 * 指定一个操作数据库的 Mapper,
 * (使用注解方式)
 * @author xiaoming
 * @date 2020-11-29 14:38
 */
@Mapper
public interface DepartmentMapper {

    @Select("select * from department where id = #{id}")
    public Department getDepartmentById(Integer id);

    @Delete("delete from department where id = #{id}")
    public int delDepartmentById(Integer id);
    @Options(useGeneratedKeys = true, keyColumn = "id")
    @Insert("insert into department (departmentName) values (#{departmentName})")
    public int insertDepartment(Department department);

    @Update("update department set departmentName = #{departmentName} where id= #{id}")
    public int updateDepartment(Department department);
}
