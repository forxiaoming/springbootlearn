package com.xm.springboot.controller;

import com.xm.springboot.bean.Department;
import com.xm.springboot.mapper.DepartmentMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * Mapper  注解方式查询数据
 */
@RestController
public class DepartmentController {
//    @Autowired
    DepartmentMapper departmentMapper;

    @GetMapping("/dept/{id}")
    public Department getDepartment(@PathVariable("id") Integer id) {

        return departmentMapper.getDepartmentById(id);
    }

    @GetMapping("/dept")
    public Department insertDepart(Department department) {
        int id = departmentMapper.insertDepartment(department);

        return departmentMapper.getDepartmentById(id);
    }
}
