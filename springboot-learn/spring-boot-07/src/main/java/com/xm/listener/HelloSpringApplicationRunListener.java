package com.xm.listener;

import org.springframework.boot.ConfigurableBootstrapContext;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringApplicationRunListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

/**
 * @author xiaoming
 * @date 2021-03-20 14:11
 */
public class HelloSpringApplicationRunListener implements SpringApplicationRunListener {
    public HelloSpringApplicationRunListener(SpringApplication application, String[] args) {

    }

    /**
     * 开始启动
     * @param bootstrapContext
     */
    @Override
    public void starting(ConfigurableBootstrapContext bootstrapContext) {
        System.out.println("HelloSpringApplicationRunListener ... staring");
    }

    /**
     * Environment 构建完成
     * @param bootstrapContext
     * @param environment
     */
    @Override
    public void environmentPrepared(ConfigurableBootstrapContext bootstrapContext, ConfigurableEnvironment environment) {
        System.out.println("HelloSpringApplicationRunListener ... environmentPrepared");
    }

    /**
     * ApplicationContext 构建完成
     * @param context
     */
    @Override
    public void contextPrepared(ConfigurableApplicationContext context) {
        System.out.println("HelloSpringApplicationRunListener ... contextPrepared");

    }

    /**
     * ApplicationContext 加载完成
     * @param context
     */
    @Override
    public void contextLoaded(ConfigurableApplicationContext context) {
        System.out.println("HelloSpringApplicationRunListener ... contextLoaded");

    }

    /**
     * ApplicationContext 完成刷新并启动
     * @param context
     */
    @Override
    public void started(ConfigurableApplicationContext context) {
        System.out.println("HelloSpringApplicationRunListener ... started");

    }

    /**
     * ApplicationContext 启动完成 (运行中)
     * @param context
     */
    @Override
    public void running(ConfigurableApplicationContext context) {
        System.out.println("HelloSpringApplicationRunListener ... running");

    }

    /**
     * ApplicationContext 启动失败
     */
    @Override
    public void failed(ConfigurableApplicationContext context, Throwable exception) {
        System.out.println("HelloSpringApplicationRunListener ... failed");

    }
}
