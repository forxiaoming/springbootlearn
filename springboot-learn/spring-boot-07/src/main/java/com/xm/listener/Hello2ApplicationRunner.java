package com.xm.listener;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author xiaoming
 * @date 2021-03-20 14:52
 */
@Component // 放在 ioc 容器中
@Order(2)  // 顺序
public class Hello2ApplicationRunner implements ApplicationRunner {
    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("HelloApplicationRunner ... run  数据第二次初始化");
    }
}
