package com.xm.listener;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author xiaoming
 * @date 2021-03-20 13:51
 */
public class HelloApplicationContextInitializer implements ApplicationContextInitializer {
    public HelloApplicationContextInitializer() {
    }

    public HelloApplicationContextInitializer(ApplicationContext context) {

    }

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {

        System.out.println("HelloApplicationContextInitializer .... run");
    }
}