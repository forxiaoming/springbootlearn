package helo.async;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.Future;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class AsyncTaskTest {
    @Autowired
    AsyncTask asyncTask;

    @Test
    /**
     * 测试同步任务
     */
    public void testSyncTask() throws Exception {
        asyncTask.doTaskOne();
        asyncTask.doTaskTwo();
        asyncTask.doTaskThree();
    }

    /**
     * 测试异步任务
     * 1. 未开启注解@EnableAsync , 直接按顺序执行了任务;
     * 2. 开启注解后, 发现没有任何输出; 因为主函数没有关心异步任务是否结束, 程序已经自动结束
     */
    @Test
    public void testAsyncTask() throws Exception {

        asyncTask.doAsyncTaskOne();
        asyncTask.doAsyncTaskTwo();
        asyncTask.doAsyncTaskThree();
    }

    /**
     * 异步回调
     * 使用Future<T>来返回异步调用的结果
     * CompletableFuture jdk8 中可代替Future
     */
    @Test
    public void testFutureTask() throws Exception {
        long start = System.currentTimeMillis();

        Future<String> task1 = asyncTask.doAsyncOne();
        Future<String> task2 = asyncTask.doAsyncTwo();
        Future<String> task3 = asyncTask.doAsyncThree();

        while (true) {
            if (task1.isDone() && task2.isDone() && task3.isDone()) {
                break;
            }
            Thread.sleep(1000);
        }

        long end = System.currentTimeMillis();

        System.out.println("任务全部完成, 耗时: " + (end - start) + "毫秒");
    }

}