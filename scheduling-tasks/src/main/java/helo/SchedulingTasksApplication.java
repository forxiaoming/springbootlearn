package helo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @EnableScheduling 启动后台任务执行
 * @EnableAsync 启用异步任务
 */
@SpringBootApplication
//@EnableScheduling
@EnableAsync
public class SchedulingTasksApplication {
    /**
     * 定时任务
     */
    public static void main(String[] args) {
        SpringApplication.run(SchedulingTasksApplication.class, args);
    }

}

