package helo.async;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import java.util.Random;
import java.util.concurrent.Future;

/**
 * @author xiaoming
 * @version 1.0
 * @date 2019-01-13 11:23
 **/
@Component
public class AsyncTask {
    // 异步调用: 按顺序开始执行, 下一个任务不用等到上一个任务结束
    // 同步调用: 按顺序开始执行, 下一个任务必须等到上一个任务结束后才能开始;

    public static Random random = new Random();

    // 同步调用任务:

    public void doTaskOne() throws Exception {
        System.out.println("开始任务一");
        long start = System.currentTimeMillis();
        Thread.sleep(1000);
        long end = System.currentTimeMillis();
        System.out.println("任务二完成, 耗时:" + (end - start) + "毫秒");
    }

    public void doTaskTwo() throws Exception {
        System.out.println("开始任务二");
        long start = System.currentTimeMillis();
        Thread.sleep(2000);
        long end = System.currentTimeMillis();
        System.out.println("任务二完成, 耗时:" + (end - start) + "毫秒");
    }

    public void doTaskThree() throws Exception {
        System.out.println("开始任务三");
        long start = System.currentTimeMillis();
        Thread.sleep(3000);
        long end = System.currentTimeMillis();
        System.out.println("任务三完成, 耗时:" + (end - start) + "毫秒");
    }


    // 使用@Async 完成异步任务

    @Async
    public void doAsyncTaskOne() throws Exception {
        System.out.println("开始异步任务一");
        long start = System.currentTimeMillis();
        Thread.sleep(1000);
        long end = System.currentTimeMillis();
        System.out.println("异步任务二完成, 耗时:" + (end - start) + "毫秒");
    }

    @Async
    public void doAsyncTaskTwo() throws Exception {
        System.out.println("开始异步任务二");
        long start = System.currentTimeMillis();
        Thread.sleep(2000);
        long end = System.currentTimeMillis();
        System.out.println("异步任务二完成, 耗时:" + (end - start) + "毫秒");
    }

    @Async
    public void doAsyncTaskThree() throws Exception {
        System.out.println("开始异步任务三");
        long start = System.currentTimeMillis();
        Thread.sleep(3000);
        long end = System.currentTimeMillis();
        System.out.println("异步任务三完成, 耗时:" + (end - start) + "毫秒");
    }

    // 异步回调
    @Async
    public Future<String> doAsyncOne() throws Exception {
        System.out.println("开始异步任务一");
        long start = System.currentTimeMillis();
        Thread.sleep(1000);
        long end = System.currentTimeMillis();
        System.out.println("异步任务一完成, 耗时:" + (end - start) + "毫秒");

        return new AsyncResult<>("异步任务一完成");
    }

    @Async
    public Future<String> doAsyncTwo() throws Exception {
        System.out.println("开始异步任务二");
        long start = System.currentTimeMillis();
        Thread.sleep(2000);
        long end = System.currentTimeMillis();
        System.out.println("异步任务二完成, 耗时:" + (end - start) + "毫秒");

        return new AsyncResult<>("异步任务二完成");
    }

    @Async
    public Future<String> doAsyncThree() throws Exception {
        System.out.println("开始异步任务三");
        long start = System.currentTimeMillis();
        Thread.sleep(3000);
        long end = System.currentTimeMillis();
        System.out.println("异步任务三完成, 耗时:" + (end - start) + "毫秒");

        return new AsyncResult<>("异步任务三完成");
    }
}
