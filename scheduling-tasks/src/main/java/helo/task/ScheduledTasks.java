package helo.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author xiaoming
 * @version 1.0
 * @date 2019-01-12 21:24
 **/
@Component
public class ScheduledTasks {
    public static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);
    public static final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Scheduled(fixedRate = 5000)
    public void reportCurrentTime(){
        /*
        1. 使用 fixedRate等构建简单定时任务
        2. 使用 @Scheduled(cron=". . .")构建复杂表达式

        @Scheduled(fixedRate = 5000) ：上一次开始执行时间点之后5秒再执行
        @Scheduled(fixedDelay = 5000) ：上一次执行完毕时间点之后5秒再执行
        @Scheduled(initialDelay=1000, fixedRate=5000) ：第一次延迟1秒后执行，之后按fixedRate的规则每5秒执行一次
        @Scheduled(cron=” /5 “) ：通过cron表达式定义规则，
        [corn 参考文档](https://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/scheduling/support/CronSequenceGenerator.html)

         */
        logger.info("当前时间: {}", df.format(new Date()));
    }
}
