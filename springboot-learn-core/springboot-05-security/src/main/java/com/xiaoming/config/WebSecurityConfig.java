package com.xiaoming.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Configuring Spring Boot Security
 * @author xiaoming
 * @date 2021-05-12 22:56
 */
// Spring Security v5.4 之前的旧方法
// public class WebSecurityConfig extends WebSecurityConfigurerAdapter   {
// 性

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/level1/**").hasRole("VIP1")
                .antMatchers("/level2/**").hasRole("VIP2")
                .antMatchers("/level3/**").hasRole("VIP3")
                .and()
            // 定制授权规则
                // 可以正常访问
                // 需要拥有角色的用户访问
            .formLogin()
//                .loginPage("/userlogin")
//                .failureForwardUrl("/userlogin")
                .loginProcessingUrl("/login")
//                .successForwardUrl("/")
                .and()
            // 定制表单登录
                // 自定义登录页面, 默认: "/login",
                // 登录失败重定向, 默认: "/login?error"
                // 登录处理, 默认: "/login"; 一旦定制了 loginPage, 则 loginPage 的 post 默认为登录处理
                // 登录成功重定向

            .logout()
                .logoutSuccessUrl("/userlogin")
                .and()

            // 定义注销
                // 注销成功后重定向页面
            .rememberMe()
                .rememberMeParameter("remeber");
//                .alwaysRemember(true);
            // 定制记住我功能
                // 自定义记住我参数
                // 总是记住我

/* 定制默认登录-Impact on other defaults
Updating this value, also impacts a number of other default values. For example, the following are the default values
when only formLogin() was specified.
* /login GET - the login form
* /login POST - process the credentials and if valid authenticate the user
* /login?error GET - redirect here for failed authentication attempts
* /login?logout GET - redirect here after successfully logging out

If "/authenticate" was passed to this method it update the defaults as shown below:
* /authenticate GET - the login form
* /authenticate POST - process the credentials and if valid authenticate the user
* /authenticate?error GET - redirect here for failed authentication attempts
* /authenticate?logout GET - redirect here after successfully logging out

Overrides:
loginPage in class AbstractAuthenticationFilterConfigurer<H extends HttpSecurityBuilder<H>,FormLoginConfigurer<H extends HttpSecurityBuilder<H>>,org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter>

Parameters:
loginPage - the login page to redirect to if authentication is required (i.e. "/login")

Returns:
the FormLoginConfigurer for additional customization
 */

    }


    // 定义认证规则
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // 2.x 需要使用密码编码器
        PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();

        // super.configure(auth);
        auth.inMemoryAuthentication()
                .withUser("zhangsang")
                .password(encoder.encode("123456"))
                .roles("VIP1")

                .and()
                .withUser("lisi")
                .password(encoder.encode("123456"))
                .roles("VIP2")

                .and()
                .withUser("wangwu")
                .password(encoder.encode("123456"))
                .roles("VIP3");
    }


/*    @Bean
    @Override
    public UserDetailsService userDetailsService() {
        UserDetails user =
                User.withDefaultPasswordEncoder()       // 默认密码编码器
                        .username("user")
                        .password("password")
                        .roles("USER")
                        .build();

        return new InMemoryUserDetailsManager(user);
    }*/
}