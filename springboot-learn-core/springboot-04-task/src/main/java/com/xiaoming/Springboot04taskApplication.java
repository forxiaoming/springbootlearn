package com.xiaoming;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling // 启动定时任务
@EnableAsync	// 启用异步任务
@SpringBootApplication
public class Springboot04taskApplication {

	public static void main(String[] args) {
		SpringApplication.run(Springboot04taskApplication.class, args);
	}

}
