package com.xiaoming.task.service;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @author xiaoming
 * @date 2021-05-07 21:32
 */
@Service
public class AsyncService {
    @Async
    public void task() {

        try {
            TimeUnit.SECONDS.sleep(8);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("异步任务执行完成");
    }
}
