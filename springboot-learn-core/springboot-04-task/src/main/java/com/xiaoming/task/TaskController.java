package com.xiaoming.task;

import com.xiaoming.task.service.AsyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xiaoming
 * @date 2021-05-06 22:06
 */
@RestController
public class TaskController {

    @Autowired
    AsyncService asyncService;


    @GetMapping("test")
    public String testAsyncTask() {

        asyncService.task();

        return "执行完成";
    }


}
