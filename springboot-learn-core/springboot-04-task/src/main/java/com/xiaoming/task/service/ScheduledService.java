package com.xiaoming.task.service;

import org.springframework.stereotype.Service;

import java.util.logging.Logger;

/**
 * 定时任务
 *
 * @author xiaoming
 * @date 2021-05-06 23:04
 */
@Service
public class ScheduledService {
    private static final Logger logger = Logger.getLogger(ScheduledService.class.getName());
    /**
     * A cron-like expression, extending the usual UN*X definition to include triggers  on the
     * second, minute, hour, day of month, month, and day of week.
     * For example, {@code "0 * * * * MON-FRI"} means once per minute on weekdays
     * 	 * (at the top of the minute - the 0th second).
     *
     */
//    @Scheduled(cron = "* * * * * MON-FRI") // 周一到周五, 每一秒
//    @Scheduled(cron = "0,1,2,4,8 * * * * MON-FRI") // 周一到周五, 指定秒执行
//    @Scheduled(cron = "0-4 * * * * MON-FRI") // 0 到 4 秒执行一次
//    @Scheduled(cron = "0/4 * * * * MON-FRI") // 0 秒启动, 4 秒间隔执行一次
    public void test() {
        logger.info("定时任务...执行");
    }
}

