package com.xiaoming;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;

@SpringBootTest
class Springboot04taskApplicationTests {
	@Autowired
	JavaMailSender mailSender;

	/** 发送简单邮件 */
	@Test
	void sendSimpleMailMessage() {
		SimpleMailMessage message = new SimpleMailMessage();
		// 邮件设置
		message.setSubject("通知-SpringBoot 邮件功能开发");
		message.setText("邮件内容测试");
		message.setFrom("123@qq.com");
		message.setTo("456@qq.com");

		mailSender.send(message);
	}

	/** 发送高级邮件 */
	@Test
	void sendMailMessage() throws MessagingException {
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);

		// 邮件设置
		helper.setSubject("通知-SpringBoot 邮件功能开发");
		helper.setText("<div  style='color:#f00;font-size: 20px;'>复杂的邮件内容测试</div>");
		helper.setFrom("123@qq.com");
		helper.setTo("456@qq.com");

		// 上传文件
		helper.addAttachment("1.jpg", new File("E:\\轻量Serverless.png"));

		mailSender.send(mimeMessage);
	}
}
