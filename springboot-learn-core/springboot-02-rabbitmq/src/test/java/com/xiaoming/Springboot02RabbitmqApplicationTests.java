package com.xiaoming;

import com.xiaoming.vo.Book;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest
class Springboot02RabbitmqApplicationTests {

    @Autowired
    RabbitTemplate rabbitTemplate;


    @Test
    void test() {

//		rabbitTemplate.convertAndSend("exchange.direct", "chatonline", new Book("三国演义", "罗贯中"));
        rabbitTemplate.convertAndSend("exchange.direct", "chatonline.news", new Book("三国演义", "罗贯中"));
//		rabbitTemplate.convertAndSend("exchange.direct", "chatonline.emps", new Book("三国演义", "罗贯中"));
    }

    /**
     * 1. 单播 (点对点)
     */
    @Test
    void contextLoads() {
        // 1. Message 需要自己构造, 定义消息体和消息头
//		rabbitTemplate.send(String exchange, String routingKey, Message message);


        // 2. Object 默认当作消息体, 默认经过序列化后(JDK )后发送给 Rabbitmq
        // 自定义 MessageConverter 实现 json
        //  convertAndSend(String exchange, String routingKey, Object object)

        Map<String, Object> map = new HashMap<>();
        map.put("msg", "Rabbit 消息测试");
        map.put("data", Arrays.asList(12, 343));
//		rabbitTemplate.convertAndSend(String exchange, String routingKey,Object);
        rabbitTemplate.convertAndSend("exchange.direct", "chatonline", map);

    }

    @Test
    void fanout() {
        rabbitTemplate.convertAndSend("exchange.fanout", "", new Book("三国演义", "罗贯中"));
    }


    /**
     * 接受数据
     */
    @Test
    void receive() {
        Object o = rabbitTemplate.receiveAndConvert("chatonline");
        System.out.println(o.getClass());
        System.out.println(o);

        // 接受数据反序列化 (通过自定义配置 MessageConverter)
		/* 控制台打印:
		class java.util.HashMap
		{msg=Rabbit, data=[12, 343]}
		*/
    }

    @Autowired
    RabbitAdmin rabbitAdmin;
//    AmqpAdmin

    /**
     * Rabbit 系统管理组件
     */
    @Test
    public void rabbitAdminOperate() {
        rabbitAdmin.declareExchange(new DirectExchange("directexchang.mycreate"));
        rabbitAdmin.declareQueue(new Queue("test.admin.rabbit", true));
        // 创建绑定规则
        rabbitAdmin.declareBinding(new Binding("test.admin.rabbit", Binding.DestinationType.QUEUE,
                "directexchang.mycreate", "test.admin.rabbit", null));
    }


}
