package com.xiaoming;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableRabbit // 启用 Rabbit
@SpringBootApplication
public class Springboot02RabbitmqApplication {

	public static void main(String[] args) {
		SpringApplication.run(Springboot02RabbitmqApplication.class, args);
	}

}
