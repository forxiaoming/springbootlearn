package com.xiaoming.service;

import com.xiaoming.vo.Book;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

/**
 * @author xiaoming
 * @date 2021-05-03 9:23
 */
@Service
public class BookService {

    @RabbitListener(queues = {"chatonline"})
    public void receiveMessage(Message message) {
        System.out.println("收到消息: " + message.getBody());
        System.out.println("收到消息: " + message.getMessageProperties());
    }

    @RabbitListener(queues = {"chatonline.news"})
    public void receiveBook(Book book) {
        System.out.println("收到消息: " + book);
    }
}
