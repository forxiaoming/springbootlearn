package com.xiaoming;

import com.xiaoming.bean.Employee;
import com.xiaoming.mapper.EmployeeMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * 缓存测试
 * @author xiaoming
 * @date 2021-04-10 10:30
 */
@SpringBootTest
public class SpringbootCacheTests {
    @Autowired
    EmployeeMapper employeeMapper;

    /** 操作 k-v 都是字符串 */
    @Autowired
    StringRedisTemplate stringRedisTemplate;

    /** k-v 都是对象 */
    @Autowired
    RedisTemplate redisTemplate;

    /**
     * 自定义 redisTemplate
     */
    @Autowired
    RedisTemplate myRedisTemplate;

    /** 测试保存字符串 */
    @Test
    public void redisTest() {
        stringRedisTemplate.opsForValue().set("name", "xiaoming");
        stringRedisTemplate.opsForValue().append("name", " 测试redis");

        System.out.println(stringRedisTemplate.opsForValue().get("name"));

        // list
        stringRedisTemplate.opsForList().leftPush("leftPush", "push");

/*
        stringRedisTemplate.opsForValue();
        stringRedisTemplate.opsForList();
        stringRedisTemplate.opsForSet();
        stringRedisTemplate.opsForHash();
        stringRedisTemplate.opsForZSet();
*/

    }

    /** 测试保存对象 */
    @Test
    public void redisTestForObj() {
        Employee employee = employeeMapper.getEmployeeById(1);
        // 1. 默认保存对象, 使用 jdk 序列化(实现序列化) 后保存到 redis
//        redisTemplate.opsForValue().set("emp1", employee);
//        redisTemplate.opsForValue().set("emp1", String.valueOf(employee));
        // 2. 以 json 方式保存数据
        // 2.1 自己将对象手动转换为 json
        // 2.2 redisTemplate 默认的序列化规则: 改变默认规则
        myRedisTemplate.opsForValue().set("emp01", employee);


    }


}
