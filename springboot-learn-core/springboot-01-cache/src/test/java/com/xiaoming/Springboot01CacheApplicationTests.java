package com.xiaoming;

import com.xiaoming.bean.Employee;
import com.xiaoming.mapper.EmployeeMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Springboot01CacheApplicationTests {

	@Autowired
	EmployeeMapper employeeMapper;

	@Test
	void contextLoads() {
		Employee employee = employeeMapper.getEmployeeById(1);

		System.out.println(employee);

	}

}
