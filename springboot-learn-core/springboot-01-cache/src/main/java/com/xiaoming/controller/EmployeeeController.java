package com.xiaoming.controller;

import com.xiaoming.bean.Employee;
import com.xiaoming.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xiaoming
 * @date 2021-03-24 21:29
 */
@RestController
public class EmployeeeController {

    @Autowired
    EmployeeService employeeService;

    @GetMapping("/emp/{id}")
    public Employee getEmployee(@PathVariable Integer id) {
        return employeeService.getEmp(id);
    }

    @PostMapping("/emp/{id}")
    public Map<String, Object> updateEmp(@RequestBody Employee employee, @PathVariable(name = "id") Integer id){
        employee.setId(id);

        System.out.println(employee);

        Employee data = employeeService.update(employee);

        Map<String, Object> result = new HashMap<>();
        result.put("code", 1000);
        result.put("msg", "修改完成");
        result.put("data", data);
        return result;
    };



}
