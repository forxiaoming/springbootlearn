package com.xiaoming.mapper;

import com.xiaoming.bean.Employee;
import org.apache.ibatis.annotations.*;

/**
 * @author xiaoming
 * @date 2021-03-24 6:14
 */
@Mapper
public interface EmployeeMapper {

    @Select("select * from employee where id = #{id}")
    public Employee getEmployeeById(Integer id);

    @Insert("insert into employee (lastName, email, gender, d_id) values (#{lastName}, #{email}, #{gender}, #{d_id})")
    public void insertEmployee(Employee emp);


    @Delete("delete from employee where id=#{id}")
    public void deleteEmployeeById(Integer id);

    @Update("update employee set lastName=#{lastName}, email=#{email}, gender=#{gender}, d_id=#{dId} where id=#{id}")
    public void updateEmployee(Employee emp);

    @Select("")
    public Employee getEmployeeByName(String name);
}
